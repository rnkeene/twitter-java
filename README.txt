LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_http2_module modules/mod_proxy_http2.so

DocumentRoot "C:\twitter4j\www"
<Directory "C:\twitter4j\www">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

<VirtualHost *:80>
    ProxyPreserveHost On

    ProxyPass /rest http://127.0.0.1:13370/rest
    ProxyPassReverse /rest http://127.0.0.1:13370/rest
</VirtualHost>

mvn exec:java -D"exec.mainClass"="ass.ass.inate.rest.MessageRestService"