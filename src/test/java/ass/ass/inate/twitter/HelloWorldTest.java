/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.twitter;

import junit.framework.TestCase;

/**
 *
 * @author rnkee
 */
public class HelloWorldTest extends TestCase {
    
    public HelloWorldTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    
    public void testHello() {
        SocialMessage hello = new SocialMessage();
        hello.write("JUnit Test.");
    }
}
