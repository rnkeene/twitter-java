/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.entity.dao.service;

import ass.ass.inate.entity.DatabaseConfiguration;
import ass.ass.inate.entity.User;
import java.util.List;
import junit.framework.TestCase;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author rnkee
 */
public class EntityTest extends TestCase {

    public static void testEntity() {
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(DatabaseConfiguration.class);

        UserService personService = context.getBean(UserService.class);

        // Add Persons
        User usr = new User();
        usr.setEmail("any@email.com");
        usr.setFamilyName("family name");
        usr.setFullName("full name");
        usr.setGivenName("given name");
        usr.setImageURL("http://img.url/");
        usr.setGoogleId("this is unique...");
        personService.add(usr);

        // Get Persons
        List<User> persons = personService.listUsers();
        for (User person : persons) {
            System.out.println("Id = " + person.getId());
            System.out.println("First Name = " + person.getFullName());
            System.out.println("Last Name = " + person.getFamilyName());
            System.out.println("First Name = " + person.getGivenName());
            System.out.println("Last Name = " + person.getImageURL());
            System.out.println("Email = " + person.getEmail());
            System.out.println();
        }
        User nextUsr = new User();
        nextUsr.setEmail("any@emailb.com");
        nextUsr.setFamilyName("family bname");
        nextUsr.setFullName("full bname");
        nextUsr.setGivenName("given bname");
        nextUsr.setImageURL("http://bimg.url/");
        nextUsr.setGoogleId("this is unique...");
        personService.add(nextUsr);
        
        persons = personService.listUsers();
        for (User person : persons) {
            System.out.println("Id = " + person.getId());
            System.out.println("First Name = " + person.getFullName());
            System.out.println("Last Name = " + person.getFamilyName());
            System.out.println("First Name = " + person.getGivenName());
            System.out.println("Last Name = " + person.getImageURL());
            System.out.println("Email = " + person.getEmail());
            System.out.println();
        }
        
        context.close();
    }
}
