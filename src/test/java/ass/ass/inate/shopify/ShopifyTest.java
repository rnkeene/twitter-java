/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.shopify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import junit.framework.TestCase;

/**
 *
 * @author rnkee
 */
public class ShopifyTest extends TestCase {

    public ShopifyTest(String testName) {
        super(testName);
    }

    private final String SHOPIFY_API = System.getenv("shopify_admin_api_key");
    private final String SHOPIFY_PWD = System.getenv("shopify_admin_password");
    private static final String DOMAIN = "https://keenedom.myshopify.com";
    private static final String PRODUCTS = DOMAIN + "/admin/products.json";
    private static final String CHECKOUT = DOMAIN + "/admin/checkouts.json";
    private static final String ACCESS_SCOPES = DOMAIN + "/admin/oauth/access_scopes.json";
    private static final String COMPLETE_CHECKOUT = DOMAIN + "/admin/checkouts/";
    
    public void testHello() throws MalformedURLException, IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(PRODUCTS);
        String loginPassword = SHOPIFY_API +":"+SHOPIFY_PWD;

        String encoded = new String(Base64.getEncoder().encode(loginPassword.getBytes()));
//        URLConnection conn = url.openConnection();

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Basic " + encoded);
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
    }
    
    public void testAccessScopes() throws MalformedURLException, IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(ACCESS_SCOPES);
        String loginPassword = SHOPIFY_API +":"+SHOPIFY_PWD;

        String encoded = new String(Base64.getEncoder().encode(loginPassword.getBytes()));
//        URLConnection conn = url.openConnection();

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Basic " + encoded);
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
    }
    
    public void testPost() throws MalformedURLException, IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(CHECKOUT);
        String loginPassword = SHOPIFY_API +":"+SHOPIFY_PWD;

        String encoded = new String(Base64.getEncoder().encode(loginPassword.getBytes()));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Basic " + encoded);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        byte[] out = "{\"checkout\": {\"line_items\": [{\"id\": 1329279107176,\"quantity\": 1}]}}".getBytes(StandardCharsets.UTF_8);
        int length = out.length;

        conn.setFixedLengthStreamingMode(length);
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.connect();
        try(OutputStream os = conn.getOutputStream()) {
            os.write(out);
        }
        System.out.println(conn.getResponseMessage());
        System.out.println("ERROR STREAM");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
        
        System.out.println("INPUT STREAM");
        rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
    }
    
    public void testCompleteCheckout() throws MalformedURLException, IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(COMPLETE_CHECKOUT+"cd45882a71568877fcc5a3e703b7e51c.json");
        String loginPassword = SHOPIFY_API +":"+SHOPIFY_PWD;

        String encoded = new String(Base64.getEncoder().encode(loginPassword.getBytes()));

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Basic " + encoded);
        conn.setRequestMethod("GET");
        System.out.println(conn.getResponseMessage());
        System.out.println("testCompleteCheckout ERROR STREAM");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
        
        System.out.println("testCompleteCheckout INPUT STREAM");
        rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        System.out.println(result.toString());
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
