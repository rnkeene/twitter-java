/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.shopify;

import java.io.IOException;
import java.net.MalformedURLException;
import junit.framework.TestCase;

/**
 *
 * @author rnkee
 */
public class GraphQLTest extends TestCase {

    public GraphQLTest(String testName) {
        super(testName);
    }

    public void testGraphQLCheckoutCreate() throws MalformedURLException, IOException {        
        System.out.println(ShopifyService.createCheckout());
    }

    public void testCheckoutConfirmation() throws MalformedURLException, IOException {
        String results = ShopifyService.confirmCheckout("Z2lkOi8vc2hvcGlmeS9DaGVja291dC85YWQ3ZWM0ODI5NWJlYjg3MjJlZjQzZTg3MTAwYmRkZj9rZXk9ZWEzZTBmZGM0OWRjNTE3Nzg1YmM0ZTdmNmY3ZWRkZDY=");
        System.out.println("Success? " + (results.length() > 350));
        System.out.println(results);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
