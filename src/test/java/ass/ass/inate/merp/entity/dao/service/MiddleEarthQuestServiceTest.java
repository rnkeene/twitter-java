/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.entity.DatabaseConfiguration;
import ass.ass.inate.merp.entity.Book;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import java.util.List;
import junit.framework.TestCase;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Nathan Keene
 */
public class MiddleEarthQuestServiceTest extends TestCase {

    public static void testEntity() {
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(DatabaseConfiguration.class);

        MiddleEarthQuestService meQuestService = context.getBean(MiddleEarthQuestService.class);

        // Add Persons
        Book book = new Book();
        book.setName("Middle Earth Quest: Test");
        meQuestService.addBook(book);

        Page prologue = meQuestService.getStartPage(book.getId());
        
        
        Page page = new Page();
        page.setBook(book);
        page.setContent("This is a paragraph of the adventure.");
        page.setPageLabel("nk1");        
        meQuestService.addPage(page);

        Page pageB = new Page();
        pageB.setBook(book);
        pageB.setContent("This is the next paragraph of the adventure.");
        pageB.setPageLabel("nk2");
        meQuestService.addPage(pageB);
        
        NextPage init = new NextPage();
        init.setFrom(prologue);
        init.setTo(page);
        meQuestService.addNextPage(init);
        
        NextPage next = new NextPage();
        next.setFrom(page);
        next.setTo(pageB);
        meQuestService.addNextPage(next);
        
        // Get Persons
        List<Book> books = meQuestService.getBooks();
        for (Book abook : books) {
            System.out.println("Id = " + abook.getId());
            System.out.println("Book Name = " + abook.getName());
            List<NextPage> nextPages = meQuestService.getNextPages(abook.getId());
            for(NextPage nxt : nextPages){
                System.out.println(nxt);
                System.out.println(nxt.getFrom());
                System.out.println(nxt.getTo());
            }
            System.out.println();
        }

        List<Page> pages = meQuestService.getPages();
        for (Page apage : pages) {
            System.out.println("Id = " + apage.getId());
            System.out.println("Page Label = " + apage.getPageLabel());
            System.out.println("Page Content = " + apage.getContent());
            System.out.println();
        }
        Page start = meQuestService.getStartPage(book.getId());
        System.out.println("Start Page Id = " + start.getId());
        System.out.println("Start Page Page Label = " + start.getPageLabel());
        System.out.println("Start Page Page Content = " + start.getContent());
        System.out.println("Start Page Book id = " + start.getBook().getId());
        System.out.println("Start Page Book name = " + start.getBook().getName());
        
        Graph<Page, DefaultEdge> g = MiddleEarthQuestGraph.createStory(book.getId());
        System.out.println("The Graph :: " + g);
        context.close();
    }
}
