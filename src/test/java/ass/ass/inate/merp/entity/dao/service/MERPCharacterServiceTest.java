/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.entity.DatabaseConfiguration;
import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.MERPHome;
import ass.ass.inate.merp.entity.MERPRace;
import java.util.List;
import junit.framework.TestCase;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Nathan Keene
 */
public class MERPCharacterServiceTest extends TestCase {

    public static void testEntity() {
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(DatabaseConfiguration.class);

        MERPCharacterService meQuestService = context.getBean(MERPCharacterService.class);

        MERPRace race = meQuestService.getRace("Noldo");        
        System.out.println(race);
        
        MERPHome home = meQuestService.getHome("Slopes of Dorthonion");        
        System.out.println(home);
        
        MERPCharacter character = new MERPCharacter();
        character.setName("Aegnor (I)");
        character.setLevel(90l);
        character.setNotes("Aegnor was the fourth of the five children of Finfarfin and Earwen...");
        meQuestService.addCharacter(character);
        
        System.out.println(character);
        
        MERPCharacterRace characterRace = new MERPCharacterRace();
        characterRace.setCharacter(character);
        characterRace.setRace(race);
        meQuestService.addCharacterRace(characterRace);
        
        System.out.println(characterRace);
        
        MERPCharacterHome characterHome = new MERPCharacterHome();
        characterHome.setCharacter(character);
        characterHome.setHome(home);
        meQuestService.addCharacterHome(characterHome);
        
        System.out.println(characterHome);
        
        MERPCharacterName characterName = new MERPCharacterName();
        characterName.setCharacter(character);
        characterName.setName("Fell-fire");
        meQuestService.addCharacterName(characterName);
        
        System.out.println(characterName);
        
        MERPCharacterName characterNameB = new MERPCharacterName();
        characterNameB.setCharacter(character);
        characterNameB.setName("Aikanaro");
        meQuestService.addCharacterName(characterNameB);
        
        System.out.println(characterNameB);
        
        List<MERPCharacter> simpleList = meQuestService.getSimpleCharacters();
        for(MERPCharacter simple : simpleList){
            System.out.println(simple);
        }
        
        context.close();
    }
}
