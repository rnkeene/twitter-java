/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.twitter;

import java.util.logging.Level;
import java.util.logging.Logger;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author rnkee
 */
public class SocialMessage {

    private Twitter assassinatekeene;
    private Twitter phaldobuggins;
    private Twitter reueljeuex;

    public SocialMessage() {
        ConfigurationBuilder assassinatekeenecb = new ConfigurationBuilder();
        assassinatekeenecb.setDebugEnabled(Boolean.valueOf(System.getenv("twitter4j_assassinatekeene_debug")))
                .setOAuthConsumerKey(System.getenv("twitter4j_assassinatekeene_oauth_consumerKey"))
                .setOAuthConsumerSecret(System.getenv("twitter4j_assassinatekeene_oauth_consumerSecret"))
                .setOAuthAccessToken(System.getenv("twitter4j_assassinatekeene_oauth_accessToken"))
                .setOAuthAccessTokenSecret(System.getenv("twitter4j_assassinatekeene_oauth_accessTokenSecret"));

        ConfigurationBuilder phaldobugginscb = new ConfigurationBuilder();
        phaldobugginscb.setDebugEnabled(Boolean.valueOf(System.getenv("twitter4j_phaldobuggins_debug")))
                .setOAuthConsumerKey(System.getenv("twitter4j_phaldobuggins_oauth_consumerKey"))
                .setOAuthConsumerSecret(System.getenv("twitter4j_phaldobuggins_oauth_consumerSecret"))
                .setOAuthAccessToken(System.getenv("twitter4j_phaldobuggins_oauth_accessToken"))
                .setOAuthAccessTokenSecret(System.getenv("twitter4j_phaldobuggins_oauth_accessTokenSecret"));

        ConfigurationBuilder reueljeuexcb = new ConfigurationBuilder();
        reueljeuexcb.setDebugEnabled(Boolean.valueOf(System.getenv("twitter4j_reueljeuex_debug")))
                .setOAuthConsumerKey(System.getenv("twitter4j_reueljeuex_oauth_consumerKey"))
                .setOAuthConsumerSecret(System.getenv("twitter4j_reueljeuex_oauth_consumerSecret"))
                .setOAuthAccessToken(System.getenv("twitter4j_reueljeuex_oauth_accessToken"))
                .setOAuthAccessTokenSecret(System.getenv("twitter4j_reueljeuex_oauth_accessTokenSecret"));

        assassinatekeene = new TwitterFactory(assassinatekeenecb.build()).getInstance();
        phaldobuggins = new TwitterFactory(phaldobugginscb.build()).getInstance();
        reueljeuex = new TwitterFactory(reueljeuexcb.build()).getInstance();
    }

    public void write(String statusMessage) {
        try {
            assassinatekeene.updateStatus(statusMessage);
            phaldobuggins.updateStatus(statusMessage);
            reueljeuex.updateStatus(statusMessage);
        } catch (TwitterException ex) {
            Logger.getLogger(SocialMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
