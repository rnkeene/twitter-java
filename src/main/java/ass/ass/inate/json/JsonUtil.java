/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author rnkee
 */
public class JsonUtil {

    private static final ObjectMapper json = new ObjectMapper();

    public static String toJSON(Object any) {
        try {
            return json.writeValueAsString(any);
        } catch (JsonProcessingException ex) {
            return any.toString();
        }
    }

}
