/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "MERP_RACE")
public class MERPRace extends Identity implements Serializable {

    @Column(unique = true)
    private String race;

    public MERPRace() {
        super();
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

}
