/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao;

import ass.ass.inate.merp.entity.Book;
import ass.ass.inate.merp.entity.BackpackItem;
import ass.ass.inate.merp.entity.CharacterRecord;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import ass.ass.inate.merp.entity.UserPage;
import java.util.List;

/**
 *
 * @author Nathan Keene
 */
public interface MiddleEarthQuestDAO {

    void addBook(Book book);

    void addPage(Page page);

    void addUserPage(UserPage userPage);

    void addNextPage(NextPage page);
    
    void addCharacterRecord(CharacterRecord characterRecord);
    
    void addBackpackItem(BackpackItem item);
    
    List<BackpackItem> getBackpackItems(long recordId);
    
    List<BackpackItem> addBackpackItems(long recordId, List<String> items);
    
    List<Book> getBooks();

    List<Page> getPages();
    
    List<CharacterRecord> getCharacterRecords(long userId);
    
    Page getStartPage(long bookId);

    UserPage getSavedPage(long userId, long bookId);

    List<NextPage> getNextPages(long pageId);

    NextPage getSingleNextPages(long fromId, long toId);

    Book getBook(long bookId);

    Page getPage(long parentId, String pageLabel);

    UserPage getUserPage(long userId, long pageId);
    
    UserPage resetUserPage(long userId, long pageId);
    
    Page getPage(long pageId);

}
