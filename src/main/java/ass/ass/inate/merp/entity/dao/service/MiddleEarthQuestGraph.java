/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.context.ServiceBeans;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 *
 * @author Nathan Keene
 */
public class MiddleEarthQuestGraph {

    private static final Map<Long, Page> UNIQUE = Collections.synchronizedMap(new HashMap<>());

    private static final Map<Page, Page> ACYCLIC = Collections.synchronizedMap(new HashMap<>());

    public static Graph<Page, DefaultEdge> createStory(long bookId) {
        MiddleEarthQuestService meService = ServiceBeans.getMiddleEarthQuestService();
        Graph<Page, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        Page page = meService.getStartPage(bookId);
        buildStory(page, g, meService);
        return g;
    }

    private static void buildStory(Page from, Graph<Page, DefaultEdge> graph, MiddleEarthQuestService meService) {
        if (UNIQUE.containsKey(from.getId())) {
            from = UNIQUE.get(from.getId());
        } else {
            UNIQUE.put(from.getId(), from);
        }
        List<NextPage> future = meService.getNextPages(from.getId());
        for (NextPage next : future) {
            Page to = next.getTo();
            if (UNIQUE.containsKey(to.getId())) {
                to = UNIQUE.get(to.getId());
            } else {
                UNIQUE.put(to.getId(), to);
            }
            if (ACYCLIC.containsKey(from)) {
                if (ACYCLIC.get(from).equals(to)) {
                    continue;
                }
            } else{
                graph.addVertex(from);
            }
            graph.addVertex(to);
            ACYCLIC.put(from, to);
            graph.addEdge(from, to);
            buildStory(to, graph, meService);
        }
    }

}
