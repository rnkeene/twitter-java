/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import ass.ass.inate.entity.User;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * No relationship with the MERPCharacter Entity.  This is for MiddleEarthAdventure Quest characters.
 * 
 * @author Nathan Keene
 */
@Entity
@Table(name = "CHARACTER_RECORD")
public class CharacterRecord extends Identity implements Serializable {

    @Column
    private int strengthValue;

    @Column
    private int strengthBonus;

    @Column
    private int agilityValue;

    @Column
    private int agilityBonus;
    
    @Column
    private int intelligenceValue;

    @Column
    private int intelligenceBonus;
    
    @Column
    private int enduranceValue;

    @Column
    private int enduranceStrength;
    
    @Column
    private int meleeOBTotal;

    @Column
    private int meleeOBSkillBonus;
    
    @Column
    private int meleeOBStatBonus;
    
    @Column
    private int meleeOBSpecialBonus;
    
    @Column
    private int missileOBTotal;

    @Column
    private int missileOBSkillBonus;
    
    @Column
    private int missileOBStatBonus;
    
    @Column
    private int missileOBSpecialBonus;
    
    @Column
    private int dbTotal;

    @Column
    private int dbSkillBonus;
    
    @Column
    private int dbStatBonus;
    
    @Column
    private int dbSpecialBonus;
    
    @Column
    private int runningTotal;

    @Column
    private int runningSkillBonus;
    
    @Column
    private int runningStatBonus;
    
    @Column
    private int runningSpecialBonus;
    
    @Column
    private int generalTotal;

    @Column
    private int generalSkillBonus;
    
    @Column
    private int generalStatBonus;
    
    @Column
    private int generalSpecialBonus;
    
    @Column
    private int trickeryTotal;

    @Column
    private int trickerySkillBonus;
    
    @Column
    private int trickeryStatBonus;
    
    @Column
    private int trickerySpecialBonus;
    
    @Column
    private int perceptionTotal;

    @Column
    private int perceptionSkillBonus;
    
    @Column
    private int perceptionStatBonus;
    
    @Column
    private int perceptionSpecialBonus;
    
    @Column
    private int magicalTotal;

    @Column
    private int magicalSkillBonus;
    
    @Column
    private int magicalStatBonus;
    
    @Column
    private int magicalSpecialBonus;
    
    @Column
    private String cloak;
    
    @Column
    private String armor;
    
    @Column
    private String dagger;
    
    @Column
    private String belt;
    
    @Column
    private int damageTaken;
    
    @Column
    private int minutes;
    
    @Column
    private int days;
    
    @Column
    private long experiencePoints;
    
    @Column
    private String characterName;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
    
    @Lob
    private String notes;

    public CharacterRecord() {
        super();
    }
    
    public int getStrengthValue() {
        return strengthValue;
    }

    public void setStrengthValue(int strengthValue) {
        this.strengthValue = strengthValue;
    }

    public int getStrengthBonus() {
        return strengthBonus;
    }

    public void setStrengthBonus(int strengthBonus) {
        this.strengthBonus = strengthBonus;
    }

    public int getAgilityValue() {
        return agilityValue;
    }

    public void setAgilityValue(int agilityValue) {
        this.agilityValue = agilityValue;
    }

    public int getAgilityBonus() {
        return agilityBonus;
    }

    public void setAgilityBonus(int agilityBonus) {
        this.agilityBonus = agilityBonus;
    }

    public int getIntelligenceValue() {
        return intelligenceValue;
    }

    public void setIntelligenceValue(int intelligenceValue) {
        this.intelligenceValue = intelligenceValue;
    }

    public int getIntelligenceBonus() {
        return intelligenceBonus;
    }

    public void setIntelligenceBonus(int intelligenceBonus) {
        this.intelligenceBonus = intelligenceBonus;
    }

    public int getEnduranceValue() {
        return enduranceValue;
    }

    public void setEnduranceValue(int enduranceValue) {
        this.enduranceValue = enduranceValue;
    }

    public int getEnduranceStrength() {
        return enduranceStrength;
    }

    public void setEnduranceStrength(int enduranceStrength) {
        this.enduranceStrength = enduranceStrength;
    }

    public int getMeleeOBTotal() {
        return meleeOBTotal;
    }

    public void setMeleeOBTotal(int meleeOBTotal) {
        this.meleeOBTotal = meleeOBTotal;
    }

    public int getMeleeOBSkillBonus() {
        return meleeOBSkillBonus;
    }

    public void setMeleeOBSkillBonus(int meleeOBSkillBonus) {
        this.meleeOBSkillBonus = meleeOBSkillBonus;
    }

    public int getMeleeOBStatBonus() {
        return meleeOBStatBonus;
    }

    public void setMeleeOBStatBonus(int meleeOBStatBonus) {
        this.meleeOBStatBonus = meleeOBStatBonus;
    }

    public int getMeleeOBSpecialBonus() {
        return meleeOBSpecialBonus;
    }

    public void setMeleeOBSpecialBonus(int meleeOBSpecialBonus) {
        this.meleeOBSpecialBonus = meleeOBSpecialBonus;
    }

    public int getMissileOBTotal() {
        return missileOBTotal;
    }

    public void setMissileOBTotal(int missileOBTotal) {
        this.missileOBTotal = missileOBTotal;
    }

    public int getMissileOBSkillBonus() {
        return missileOBSkillBonus;
    }

    public void setMissileOBSkillBonus(int missileOBSkillBonus) {
        this.missileOBSkillBonus = missileOBSkillBonus;
    }

    public int getMissileOBStatBonus() {
        return missileOBStatBonus;
    }

    public void setMissileOBStatBonus(int missileOBStatBonus) {
        this.missileOBStatBonus = missileOBStatBonus;
    }

    public int getMissileOBSpecialBonus() {
        return missileOBSpecialBonus;
    }

    public void setMissileOBSpecialBonus(int missileOBSpecialBonus) {
        this.missileOBSpecialBonus = missileOBSpecialBonus;
    }

    public int getDbTotal() {
        return dbTotal;
    }

    public void setDbTotal(int dbTotal) {
        this.dbTotal = dbTotal;
    }

    public int getDbSkillBonus() {
        return dbSkillBonus;
    }

    public void setDbSkillBonus(int dbSkillBonus) {
        this.dbSkillBonus = dbSkillBonus;
    }

    public int getDbStatBonus() {
        return dbStatBonus;
    }

    public void setDbStatBonus(int dbStatBonus) {
        this.dbStatBonus = dbStatBonus;
    }

    public int getDbSpecialBonus() {
        return dbSpecialBonus;
    }

    public void setDbSpecialBonus(int dbSpecialBonus) {
        this.dbSpecialBonus = dbSpecialBonus;
    }

    public int getRunningTotal() {
        return runningTotal;
    }

    public void setRunningTotal(int runningTotal) {
        this.runningTotal = runningTotal;
    }

    public int getRunningSkillBonus() {
        return runningSkillBonus;
    }

    public void setRunningSkillBonus(int runningSkillBonus) {
        this.runningSkillBonus = runningSkillBonus;
    }

    public int getRunningStatBonus() {
        return runningStatBonus;
    }

    public void setRunningStatBonus(int runningStatBonus) {
        this.runningStatBonus = runningStatBonus;
    }

    public int getRunningSpecialBonus() {
        return runningSpecialBonus;
    }

    public void setRunningSpecialBonus(int runningSpecialBonus) {
        this.runningSpecialBonus = runningSpecialBonus;
    }

    public int getGeneralTotal() {
        return generalTotal;
    }

    public void setGeneralTotal(int generalTotal) {
        this.generalTotal = generalTotal;
    }

    public int getGeneralSkillBonus() {
        return generalSkillBonus;
    }

    public void setGeneralSkillBonus(int generalSkillBonus) {
        this.generalSkillBonus = generalSkillBonus;
    }

    public int getGeneralStatBonus() {
        return generalStatBonus;
    }

    public void setGeneralStatBonus(int generalStatBonus) {
        this.generalStatBonus = generalStatBonus;
    }

    public int getGeneralSpecialBonus() {
        return generalSpecialBonus;
    }

    public void setGeneralSpecialBonus(int generalSpecialBonus) {
        this.generalSpecialBonus = generalSpecialBonus;
    }

    public int getTrickeryTotal() {
        return trickeryTotal;
    }

    public void setTrickeryTotal(int trickeryTotal) {
        this.trickeryTotal = trickeryTotal;
    }

    public int getTrickerySkillBonus() {
        return trickerySkillBonus;
    }

    public void setTrickerySkillBonus(int trickerySkillBonus) {
        this.trickerySkillBonus = trickerySkillBonus;
    }

    public int getTrickeryStatBonus() {
        return trickeryStatBonus;
    }

    public void setTrickeryStatBonus(int trickeryStatBonus) {
        this.trickeryStatBonus = trickeryStatBonus;
    }

    public int getTrickerySpecialBonus() {
        return trickerySpecialBonus;
    }

    public void setTrickerySpecialBonus(int trickerySpecialBonus) {
        this.trickerySpecialBonus = trickerySpecialBonus;
    }

    public int getPerceptionTotal() {
        return perceptionTotal;
    }

    public void setPerceptionTotal(int perceptionTotal) {
        this.perceptionTotal = perceptionTotal;
    }

    public int getPerceptionSkillBonus() {
        return perceptionSkillBonus;
    }

    public void setPerceptionSkillBonus(int perceptionSkillBonus) {
        this.perceptionSkillBonus = perceptionSkillBonus;
    }

    public int getPerceptionStatBonus() {
        return perceptionStatBonus;
    }

    public void setPerceptionStatBonus(int perceptionStatBonus) {
        this.perceptionStatBonus = perceptionStatBonus;
    }

    public int getPerceptionSpecialBonus() {
        return perceptionSpecialBonus;
    }

    public void setPerceptionSpecialBonus(int perceptionSpecialBonus) {
        this.perceptionSpecialBonus = perceptionSpecialBonus;
    }

    public int getMagicalTotal() {
        return magicalTotal;
    }

    public void setMagicalTotal(int magicalTotal) {
        this.magicalTotal = magicalTotal;
    }

    public int getMagicalSkillBonus() {
        return magicalSkillBonus;
    }

    public void setMagicalSkillBonus(int magicalSkillBonus) {
        this.magicalSkillBonus = magicalSkillBonus;
    }

    public int getMagicalStatBonus() {
        return magicalStatBonus;
    }

    public void setMagicalStatBonus(int magicalStatBonus) {
        this.magicalStatBonus = magicalStatBonus;
    }

    public int getMagicalSpecialBonus() {
        return magicalSpecialBonus;
    }

    public void setMagicalSpecialBonus(int magicalSpecialBonus) {
        this.magicalSpecialBonus = magicalSpecialBonus;
    }

    public String getCloak() {
        return cloak;
    }

    public void setCloak(String cloak) {
        this.cloak = cloak;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public String getDagger() {
        return dagger;
    }

    public void setDagger(String dagger) {
        this.dagger = dagger;
    }

    public String getBelt() {
        return belt;
    }

    public void setBelt(String belt) {
        this.belt = belt;
    }

    public int getDamageTaken() {
        return damageTaken;
    }

    public void setDamageTaken(int damageTaken) {
        this.damageTaken = damageTaken;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public long getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(long experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
