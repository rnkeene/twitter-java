/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao;

import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.MERPHome;
import ass.ass.inate.merp.entity.MERPRace;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nathan Keene
 */
@Repository
public class MERPDAOImpl implements MERPDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addRace(MERPRace race) {
        entityManager.persist(race);
        entityManager.close();
    }

    @Override
    public MERPRace getRace(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPRace> criteriaQuery = criteriaBuilder.createQuery(MERPRace.class);
        Root<MERPRace> from = criteriaQuery.from(MERPRace.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("race"), name));
        MERPRace race = null;
        try {
            race = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            race = new MERPRace();
            race.setRace(name);
            addRace(race);
        }
        entityManager.close();
        return race;
    }

    @Override
    public void addHome(MERPHome home) {
        entityManager.persist(home);
        entityManager.close();
    }

    @Override
    public MERPHome getHome(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPHome> criteriaQuery = criteriaBuilder.createQuery(MERPHome.class);
        Root<MERPHome> from = criteriaQuery.from(MERPHome.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("home"), name));
        MERPHome home = null;
        try {
            home = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            home = new MERPHome();
            home.setHome(name);
            addHome(home);
        }
        entityManager.close();
        return home;
    }
    
    @Override
    public List<MERPCharacter> getSimpleCharacters(){        
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPCharacter> cq = criteriaBuilder.createQuery(MERPCharacter.class);
        Root<MERPCharacter> root = cq.from(MERPCharacter.class);
        cq.multiselect(root.get("name"), root.get("id"));
        List<MERPCharacter> result = entityManager.createQuery(cq).getResultList();
        entityManager.close();
        return result;
    }

    @Override
    public void addCharacter(MERPCharacter character) {
        if (character.getId() == null) {
            entityManager.persist(character);
        } else {
            entityManager.merge(character);
        }
        entityManager.close();
    }

    @Override
    public MERPCharacter getMERPCharacter(long id) {
        MERPCharacter character = entityManager.find(MERPCharacter.class, id);
        entityManager.close();
        return character;
    }
    
    @Override
    public void addCharacterRace(Long id, String race) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPCharacterRace> criteriaQuery = criteriaBuilder.createQuery(MERPCharacterRace.class);
        Root<MERPCharacterRace> from = criteriaQuery.from(MERPCharacterRace.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("character").<Long>get("id"), id));
        MERPCharacterRace characterRace = null;
        try {
            characterRace = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            MERPCharacter character = entityManager.find(MERPCharacter.class, id);
            characterRace = new MERPCharacterRace();
            characterRace.setCharacter(character);
            entityManager.persist(characterRace);
        }
        MERPRace merpRace = getRace(race);
        characterRace.setRace(merpRace);
        entityManager.merge(characterRace);
        entityManager.close();
    }

    @Override
    public void addCharacterNames(long id, List<String> names) {
        List<MERPCharacterName> oldItems = getCharacterNames(id);
        for (MERPCharacterName old : oldItems) {
            entityManager.remove(old);
        }
        MERPCharacter character = entityManager.find(MERPCharacter.class, id);
        for (String name : names) {
            MERPCharacterName characterName = new MERPCharacterName();
            characterName.setCharacter(character);
            characterName.setName(name);
            entityManager.persist(characterName);
        }
        entityManager.close();
    }

    @Override
    public List<MERPCharacterName> getCharacterNames(long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPCharacterName> criteriaQuery = criteriaBuilder.createQuery(MERPCharacterName.class);
        Root<MERPCharacterName> from = criteriaQuery.from(MERPCharacterName.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("character").<Long>get("id"), id));
        List<MERPCharacterName> items = entityManager.createQuery(criteriaQuery).getResultList();
        return items;
    }
    
    @Override
    public List<MERPCharacterHome> getCharacterHomes(long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPCharacterHome> criteriaQuery = criteriaBuilder.createQuery(MERPCharacterHome.class);
        Root<MERPCharacterHome> from = criteriaQuery.from(MERPCharacterHome.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("character").<Long>get("id"), id));
        List<MERPCharacterHome> items = entityManager.createQuery(criteriaQuery).getResultList();
        return items;
    }
    
    @Override
    public MERPCharacterRace getMERPCharacterRace(long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MERPCharacterRace> criteriaQuery = criteriaBuilder.createQuery(MERPCharacterRace.class);
        Root<MERPCharacterRace> from = criteriaQuery.from(MERPCharacterRace.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("character").<Long>get("id"), id));
        MERPCharacterRace race = entityManager.createQuery(criteriaQuery).getSingleResult();
        entityManager.close();
        return race;
    }
    
    @Override
    public void addCharacterHomes(long id, List<String> homes) {
        List<MERPCharacterHome> oldItems = getCharacterHomes(id);
        for(MERPCharacterHome old : oldItems){
            entityManager.remove(old);
        }
        MERPCharacter character = entityManager.find(MERPCharacter.class, id);
        for (String home : homes) {
            MERPCharacterHome characterHome = new MERPCharacterHome();
            characterHome.setCharacter(character);
            characterHome.setHome(getHome(home));
            entityManager.persist(characterHome);
        }
        entityManager.close();
    }

    @Override
    public List<MERPCharacter> getMERPCharacters() {
        CriteriaQuery<MERPCharacter> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(MERPCharacter.class);
        criteriaQuery.from(MERPCharacter.class);
        List<MERPCharacter> merpCharacters = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return merpCharacters;
    }
    
    

    @Override
    public void addCharacterHome(MERPCharacterHome home) {
        if (home.getId() == null) {
            entityManager.persist(home);
        } else {
            entityManager.merge(home);
        }
        entityManager.close();
    }

    @Override
    public void addCharacterName(MERPCharacterName name) {
        if (name.getId() == null) {
            entityManager.persist(name);
        } else {
            entityManager.merge(name);
        }
        entityManager.close();
    }

    @Override
    public void addCharacterRace(MERPCharacterRace race) {
        if (race.getId() == null) {
            entityManager.persist(race);
        } else {
            entityManager.merge(race);
        }
        entityManager.close();
    }
}
