/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "NEXT_PAGE")
public class NextPage extends Identity implements Serializable {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name="from_id", referencedColumnName="id")
    private Page from;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name="to_id", referencedColumnName="id")
    private Page to;

    public NextPage() {
        super();
    }

    public Page getFrom() {
        return from;
    }

    public void setFrom(Page from) {
        this.from = from;
    }

    public Page getTo() {
        return to;
    }

    public void setTo(Page to) {
        this.to = to;
    }

}
