/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "BACKPACK_ITEM")
public class BackpackItem extends Identity implements Serializable {
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name="record_id", referencedColumnName="id")
    private CharacterRecord record;
    
    private String item;

    public BackpackItem() {
        super();
    }

    public CharacterRecord getRecord() {
        return record;
    }

    public void setRecord(CharacterRecord record) {
        this.record = record;
    }


    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

}
