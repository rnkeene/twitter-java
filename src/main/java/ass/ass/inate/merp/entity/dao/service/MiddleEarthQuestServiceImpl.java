/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.merp.entity.BackpackItem;
import ass.ass.inate.merp.entity.Book;
import ass.ass.inate.merp.entity.CharacterRecord;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import ass.ass.inate.merp.entity.UserPage;
import ass.ass.inate.merp.entity.dao.MiddleEarthQuestDAO;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nathan Keene
 */
@Service
public class MiddleEarthQuestServiceImpl implements MiddleEarthQuestService {

    @Autowired
    private MiddleEarthQuestDAO middleEarthQuestDao;

    @Transactional
    @Override
    public void addBook(Book book) {
        middleEarthQuestDao.addBook(book);
    }

    @Transactional
    @Override
    public void addPage(Page page) {
        middleEarthQuestDao.addPage(page);
    }

    @Transactional
    @Override
    public void addNextPage(NextPage page) {
        middleEarthQuestDao.addNextPage(page);
    }
    
    @Transactional
    @Override
    public void addCharacterRecord(CharacterRecord characterRecord) {
        middleEarthQuestDao.addCharacterRecord(characterRecord);
    }
    
    @Transactional
    @Override
    public void addBackpackItem(BackpackItem item) {
        middleEarthQuestDao.addBackpackItem(item);
    }
    
    @Transactional
    @Override
    public List<BackpackItem> getBackpackItems(long recordId){
        return middleEarthQuestDao.getBackpackItems(recordId);
    }
    
    @Transactional
    @Override
    public List<BackpackItem> addBackpackItems(long recordId, List<String> items) {
        return middleEarthQuestDao.addBackpackItems(recordId, items);
    }
    
    @Transactional
    @Override
    public List<Book> getBooks() {
        return middleEarthQuestDao.getBooks();
    }

    @Transactional
    @Override
    public List<Page> getPages() {
        return middleEarthQuestDao.getPages();
    }

    @Transactional
    @Override
    public List<CharacterRecord> getCharacterRecords(Long userId){
        return middleEarthQuestDao.getCharacterRecords(userId);
    }
    
    @Transactional
    @Override
    public UserPage getSavedPage(long userId, long bookId) {
        return middleEarthQuestDao.getSavedPage(userId, bookId);
    }
    
    @Transactional
    @Override
    public UserPage getUserPage(long userId, long pageId){
        return middleEarthQuestDao.getUserPage(userId, pageId);
    }
    
    @Transactional    
    @Override
    public UserPage resetUserPage(long userId, long pageId) {        
        return middleEarthQuestDao.resetUserPage(userId, pageId);
    }
    
    @Transactional
    @Override
    public Page getStartPage(long bookId) {
        return middleEarthQuestDao.getStartPage(bookId);
    }

    @Transactional
    @Override
    public Book getBook(long bookId) {
        return middleEarthQuestDao.getBook(bookId);
    }

    @Transactional
    @Override
    public Page getPage(long parentId, String pageLabel) {
        return middleEarthQuestDao.getPage(parentId, pageLabel);
    }
    
    @Transactional
    @Override
    public Page getPage(long pageId) {        
        return middleEarthQuestDao.getPage(pageId);
    }

    @Transactional
    @Override
    public List<NextPage> getNextPages(long pageId) {
        return middleEarthQuestDao.getNextPages(pageId);
    }

}
