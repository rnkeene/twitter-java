/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * Other names a Character is known by.
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "MERP_CHARACTER_NAME")
public class MERPCharacterName extends Identity implements Serializable {

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "character_id", referencedColumnName = "id")
    private MERPCharacter character;

    @Column
    private String name;

    public MERPCharacterName() {
        super();
    }

    public MERPCharacter getCharacter() {
        return character;
    }

    public void setCharacter(MERPCharacter character) {
        this.character = character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
