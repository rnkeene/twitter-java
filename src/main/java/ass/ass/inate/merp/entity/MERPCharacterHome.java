/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "MERP_CHARACTER_HOME")
public class MERPCharacterHome extends Identity implements Serializable {

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "character_id", referencedColumnName = "id")
    private MERPCharacter character;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "home_id", referencedColumnName = "id")
    private MERPHome home;

    public MERPCharacterHome() {
        super();
    }

    public MERPCharacter getCharacter() {
        return character;
    }

    public void setCharacter(MERPCharacter character) {
        this.character = character;
    }

    public MERPHome getHome() {
        return home;
    }

    public void setHome(MERPHome home) {
        this.home = home;
    }

}
