/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.MERPHome;
import ass.ass.inate.merp.entity.MERPRace;
import ass.ass.inate.merp.entity.dao.MERPDAO;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nathan Keene
 */
@Service
public class MERPCharacterServiceImpl implements MERPCharacterService {

    @Autowired
    private MERPDAO merpDao;

    @Transactional
    @Override
    public void addRace(MERPRace race) {
        merpDao.addRace(race);
    }

    @Transactional
    @Override
    public MERPRace getRace(String race) {
        return merpDao.getRace(race);
    }

    @Transactional
    @Override
    public void addHome(MERPHome home) {
        merpDao.addHome(home);
    }

    @Transactional
    @Override
    public MERPHome getHome(String home) {
        return merpDao.getHome(home);
    }

    @Transactional
    @Override
    public MERPCharacter getMERPCharacter(long id) {
        return merpDao.getMERPCharacter(id);
    }

    @Transactional
    @Override
    public MERPCharacterRace getMERPCharacterRace(long id) {
        return merpDao.getMERPCharacterRace(id);
    }

    @Transactional
    @Override
    public List<MERPCharacterHome> getCharacterHomes(long id) {
        return merpDao.getCharacterHomes(id);
    }

    @Transactional
    @Override
    public List<MERPCharacterName> getCharacterNames(long id) {
        return merpDao.getCharacterNames(id);
    }

    @Transactional
    @Override
    public void addCharacter(MERPCharacter character) {
        merpDao.addCharacter(character);
    }

    @Transactional
    @Override
    public List<MERPCharacter> getCharacters() {
        return merpDao.getMERPCharacters();
    }

    @Transactional
    @Override
    public List<MERPCharacter> getSimpleCharacters() {
        return merpDao.getSimpleCharacters();
    }

    @Transactional
    @Override
    public void addCharacterRace(long id, String race) {
        merpDao.addCharacterRace(id, race);
    }

    @Transactional
    @Override
    public void addCharacterHome(MERPCharacterHome home) {
        merpDao.addCharacterHome(home);
    }

    @Transactional
    @Override
    public void addCharacterHomes(long id, List<String> homes) {
        merpDao.addCharacterHomes(id, homes);
    }

    @Transactional
    @Override
    public void addCharacterName(MERPCharacterName name) {
        merpDao.addCharacterName(name);
    }

    @Transactional
    @Override
    public void addCharacterNames(long id, List<String> names) {
        merpDao.addCharacterNames(id, names);
    }

    @Transactional
    @Override
    public void addCharacterRace(MERPCharacterRace race) {
        merpDao.addCharacterRace(race);
    }


}
