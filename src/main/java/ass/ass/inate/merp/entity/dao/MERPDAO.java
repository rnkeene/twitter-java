/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao;

import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.MERPHome;
import ass.ass.inate.merp.entity.MERPRace;
import java.util.List;

/**
 *
 * @author Nathan Keene
 */
public interface MERPDAO {

    void addRace(MERPRace race);
    
    MERPRace getRace(String name);
    
    void addHome(MERPHome home);
    
    MERPHome getHome(String name);

    void addCharacter(MERPCharacter character);
    
    MERPCharacter getMERPCharacter(long id);
    
    List<MERPCharacter> getSimpleCharacters();
    
    void addCharacterNames(long id, List<String> names);
    
    void addCharacterHomes(long id, List<String> homes);
    
    List<MERPCharacterName> getCharacterNames(long id);
    
    List<MERPCharacterHome> getCharacterHomes(long id);
    
    MERPCharacterRace getMERPCharacterRace(long id);
    
    void addCharacterRace(Long id, String race);
    
    List<MERPCharacter> getMERPCharacters();
    
    void addCharacterHome(MERPCharacterHome home);

    void addCharacterName(MERPCharacterName name);
    
    void addCharacterRace(MERPCharacterRace race);
    
}
