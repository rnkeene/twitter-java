/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao;

import ass.ass.inate.entity.User;
import ass.ass.inate.merp.entity.Book;
import ass.ass.inate.merp.entity.BackpackItem;
import ass.ass.inate.merp.entity.CharacterRecord;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import ass.ass.inate.merp.entity.UserPage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nathan Keene
 */
@Repository
public class MiddleEarthQuestDAOImpl implements MiddleEarthQuestDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addBook(Book book) {
        entityManager.persist(book);
        entityManager.close();
    }

    @Override
    public void addPage(Page page) {
        if (page.getId() == null) {
            entityManager.persist(page);
        } else {
            entityManager.merge(page);
        }
        entityManager.close();
    }

    @Override
    public void addUserPage(UserPage userPage) {
        entityManager.persist(userPage);
        entityManager.close();
    }

    @Override
    public void addBackpackItem(BackpackItem item) {
        if (item.getId() == null) {
            entityManager.persist(item);
        } else {
            entityManager.merge(item);
        }
        entityManager.close();
    }

    @Override
    public List<BackpackItem> addBackpackItems(long recordId, List<String> items) {
        List<BackpackItem> oldItems = getBackpackItems(recordId);
        for(BackpackItem old : oldItems){
            entityManager.remove(old);
        }
        CharacterRecord record = entityManager.find(CharacterRecord.class, recordId);
        List<BackpackItem> backpack = new ArrayList<>();
        for (String item : items) {
            backpack.add(getBackpackItem(record, item));
        }
        entityManager.close();
        return backpack;
    }

    @Override
    public List<BackpackItem> getBackpackItems(long recordId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BackpackItem> criteriaQuery = criteriaBuilder.createQuery(BackpackItem.class);
        Root<BackpackItem> from = criteriaQuery.from(BackpackItem.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("record").<Long>get("id"), recordId));
        List<BackpackItem> items = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return items;
    }

    private BackpackItem getBackpackItem(CharacterRecord record, String item) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BackpackItem> criteriaQuery = criteriaBuilder.createQuery(BackpackItem.class);
        Root<BackpackItem> from = criteriaQuery.from(BackpackItem.class);
        Predicate predicate = criteriaBuilder.equal(from.get("record").<Long>get("id"), record.getId());
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(from.get("item"), item));
        criteriaQuery.where(predicate);
        BackpackItem backpackItem = null;
        try {
            backpackItem = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            BackpackItem pack = new BackpackItem();
            pack.setRecord(record);
            pack.setItem(item);
            addBackpackItem(pack);
        }
        entityManager.close();
        return backpackItem;
    }

    @Override
    public void addNextPage(NextPage page) {
        if (page.getId() == null) {
            entityManager.persist(page);
        } else {
            entityManager.merge(page);
        }
        entityManager.close();
    }

    @Override
    public List<Book> getBooks() {
        CriteriaQuery<Book> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Book.class);
        criteriaQuery.from(Book.class);
        List<Book> books = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return books;
    }

    @Override
    public List<Page> getPages() {
        CriteriaQuery<Page> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Page.class);
        criteriaQuery.from(Page.class);
        List<Page> pages = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return pages;
    }

    @Override
    public void addCharacterRecord(CharacterRecord characterRecord) {
        if (characterRecord.getId() == null) {
            entityManager.persist(characterRecord);
        } else {
            entityManager.merge(characterRecord);
        }
        entityManager.close();
    }

    @Override
    public List<CharacterRecord> getCharacterRecords(long userId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CharacterRecord> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(CharacterRecord.class);
        Root<CharacterRecord> from = criteriaQuery.from(CharacterRecord.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("user").<Long>get("id"), userId));
        List<CharacterRecord> records = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return records;
    }

    @Override
    public Page getStartPage(long bookId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Page> criteriaQuery = criteriaBuilder.createQuery(Page.class);
        Root<Page> from = criteriaQuery.from(Page.class);
        Predicate predicate = criteriaBuilder.equal(from.get("book").<Long>get("id"), bookId);
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(from.get("pageLabel"), "prologue"));
        criteriaQuery.where(predicate);
        Page page = null;
        try {
            page = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            Book book = getBook(bookId);
            page = new Page();
            page.setPageLabel("prologue");
            page.setBook(book);
            addPage(page);
        }
        entityManager.close();
        return page;
    }

    @Override
    public UserPage getSavedPage(long userId, long bookId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserPage> criteriaQuery = criteriaBuilder.createQuery(UserPage.class);
        Root<UserPage> from = criteriaQuery.from(UserPage.class);
        Predicate predicate = criteriaBuilder.equal(from.get("user").<Long>get("id"), userId);
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(from.get("page").<Book>get("book").<Long>get("id"), bookId));
        criteriaQuery.where(predicate);
        UserPage userPage = null;
        try {
            userPage = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            userPage = new UserPage();
            userPage.setPage(getStartPage(bookId));
            userPage.setUser(entityManager.find(User.class, userId));
            addUserPage(userPage);
        }
        return userPage;
    }

    @Override
    public List<NextPage> getNextPages(long pageId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<NextPage> criteriaQuery = criteriaBuilder.createQuery(NextPage.class);
        Root<NextPage> from = criteriaQuery.from(NextPage.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("from").<Long>get("id"), pageId));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public NextPage getSingleNextPages(long fromId, long toId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<NextPage> criteriaQuery = criteriaBuilder.createQuery(NextPage.class);
        Root<NextPage> find = criteriaQuery.from(NextPage.class);
        Predicate predicate = criteriaBuilder.equal(find.get("from").<Long>get("id"), fromId);
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(find.get("to").<Long>get("id"), toId));
        criteriaQuery.where(predicate);
        NextPage nextPage = null;
        try {
            nextPage = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            nextPage = new NextPage();
            nextPage.setFrom(getPage(fromId));
            nextPage.setTo(getPage(toId));
            addNextPage(nextPage);
        }
        return nextPage;
    }

    @Override
    public Page getPage(long parentId, String pageLabel) {
        Page parent = getPage(parentId);
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Page> criteriaQuery = criteriaBuilder.createQuery(Page.class);
        Root<Page> from = criteriaQuery.from(Page.class);
        Predicate predicate = criteriaBuilder.equal(from.get("book").<Long>get("id"), parent.getBook().getId());
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(from.get("pageLabel"), pageLabel));
        criteriaQuery.where(predicate);
        Page page = null;
        try {
            page = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            page = new Page();
            page.setPageLabel(pageLabel);
            page.setBook(parent.getBook());
            addPage(page);
        }
        getSingleNextPages(parentId, page.getId());
        entityManager.close();
        return page;
    }

    @Override
    public UserPage getUserPage(long userId, long pageId) {
        Page parent = getPage(pageId);
        UserPage page = getSavedPage(userId, parent.getBook().getId());
        page.setPage(parent);
        addUserPage(page);
        return page;
    }

    @Override
    public UserPage resetUserPage(long userId, long pageId) {
        Page parent = getPage(pageId);
        Page reset = getStartPage(parent.getBook().getId());
        UserPage page = getSavedPage(userId, parent.getBook().getId());
        page.setPage(reset);
        addUserPage(page);
        return page;
    }
    
    @Override
    public Book getBook(long bookId) {
        Book book = entityManager.find(Book.class, bookId);
        entityManager.close();
        return book;
    }

    @Override
    public Page getPage(long pageId) {
        Page page = entityManager.find(Page.class, pageId);
        entityManager.close();
        return page;
    }

}
