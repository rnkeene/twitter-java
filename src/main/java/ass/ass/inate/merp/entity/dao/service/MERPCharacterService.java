/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity.dao.service;

import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.MERPHome;
import ass.ass.inate.merp.entity.MERPRace;
import java.util.List;

/**
 *
 * @author Nathan Keene
 */
public interface MERPCharacterService {

    void addRace(MERPRace race);

    void addHome(MERPHome home);

    MERPRace getRace(String race);

    MERPHome getHome(String home);

    void addCharacter(MERPCharacter character);
    
    MERPCharacter getMERPCharacter(long id);
    
    void addCharacterRace(long id, String race);

    List<MERPCharacter> getCharacters();
    
    List<MERPCharacter> getSimpleCharacters();

    void addCharacterHome(MERPCharacterHome home);

    void addCharacterHomes(long id, List<String> homes);

    MERPCharacterRace getMERPCharacterRace(long id);

    List<MERPCharacterHome> getCharacterHomes(long id);

    List<MERPCharacterName> getCharacterNames(long id);

    void addCharacterName(MERPCharacterName name);

    void addCharacterNames(long id, List<String> names);

    void addCharacterRace(MERPCharacterRace race);

}
