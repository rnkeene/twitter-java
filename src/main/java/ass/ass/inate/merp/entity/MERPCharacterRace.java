/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.merp.entity;

import ass.ass.inate.entity.Identity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nathan Keene
 */
@Entity
@Table(name = "MERP_CHARACTER_RACE")
public class MERPCharacterRace extends Identity implements Serializable {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "race_id", referencedColumnName = "id")
    private MERPRace race;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "character_id", referencedColumnName = "id")
    private MERPCharacter character;

    public MERPCharacterRace() {
        super();
    }

    public MERPRace getRace() {
        return race;
    }

    public void setRace(MERPRace race) {
        this.race = race;
    }

    public MERPCharacter getCharacter() {
        return character;
    }

    public void setCharacter(MERPCharacter character) {
        this.character = character;
    }

}
