/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.rest;

import ass.ass.inate.context.ServiceBeans;
import ass.ass.inate.entity.User;
import ass.ass.inate.merp.entity.BackpackItem;
import ass.ass.inate.merp.entity.Book;
import ass.ass.inate.merp.entity.CharacterRecord;
import ass.ass.inate.merp.entity.MERPCharacter;
import ass.ass.inate.merp.entity.MERPCharacterHome;
import ass.ass.inate.merp.entity.MERPCharacterName;
import ass.ass.inate.merp.entity.MERPCharacterRace;
import ass.ass.inate.merp.entity.NextPage;
import ass.ass.inate.merp.entity.Page;
import ass.ass.inate.merp.entity.UserPage;
import ass.ass.inate.shopify.ShopifyService;
import ass.ass.inate.twitter.SocialMessage;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Nathan Keene
 */
@Path("")
public class MessageRestService extends API {

    private final SocialMessage message = new SocialMessage();

    private final String socialSecret = System.getenv("social_secret");

    public MessageRestService() {
    }

    @GET
    @Path("/rest/{secret}")
    @Produces(MediaType.TEXT_PLAIN)
    public String socialMessage(@PathParam("secret") String secret, @QueryParam("message") String socialMessage) {
        if (secret.equals(socialSecret)) {
            message.write(socialMessage);
            return "success";
        }
        return "failure";
    }

    @POST
    @Path("/rest/user")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public User handleUser(User user) {
        return ServiceBeans.getUserService().add(user);
    }

    @POST
    @Path("/rest/book/{secret}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.TEXT_PLAIN)
    public String createBook(Book book, @PathParam("secret") String secret) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMiddleEarthQuestService().addBook(book);
            return "success";
        }
        return "403";
    }

    @POST
    @Path("/rest/{userId}/record")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.TEXT_PLAIN)
    public String createRecord(CharacterRecord record, @PathParam("userId") Long userId) {
        record.setUser(ServiceBeans.getUserService().get(userId));
        ServiceBeans.getMiddleEarthQuestService().addCharacterRecord(record);
        return record.getId().toString();
    }

    @GET
    @Path("/rest/backpack/{recordId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<BackpackItem> getBackpackItems(@PathParam("recordId") Long recordId) {
        return ServiceBeans.getMiddleEarthQuestService().getBackpackItems(recordId);
    }

    @POST
    @Path("/rest/backpack/{recordId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public List<BackpackItem> createBackpackItems(List<String> items, @PathParam("recordId") Long recordId) {
        return ServiceBeans.getMiddleEarthQuestService().addBackpackItems(recordId, items);
    }

    @GET
    @Path("/rest/{userId}/record")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public List<CharacterRecord> getRecords(@PathParam("userId") Long userId) {
        return ServiceBeans.getMiddleEarthQuestService().getCharacterRecords(userId);
    }

    @GET
    @Path("/rest/books")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Book> getBooks() {
        return ServiceBeans.getMiddleEarthQuestService().getBooks();
    }

    @GET
    @Path("/rest/page/start/{bookId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Page getStartPage(@PathParam("bookId") Long bookId) {
        return ServiceBeans.getMiddleEarthQuestService().getStartPage(bookId);
    }

    @GET
    @Path("/rest/page/start/{userId}/{bookId}")
    @Produces({MediaType.APPLICATION_JSON})
    public UserPage getUserStartPage(@PathParam("userId") Long userId, @PathParam("bookId") Long bookId) {
        return ServiceBeans.getMiddleEarthQuestService().getSavedPage(userId, bookId);
    }

    @GET
    @Path("/rest/page/{pageId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Page getPage(@PathParam("pageId") Long pageId) {
        return ServiceBeans.getMiddleEarthQuestService().getPage(pageId);
    }

    @GET
    @Path("/rest/user/{userId}/{pageId}")
    @Produces({MediaType.APPLICATION_JSON})
    public UserPage getUserPage(@PathParam("userId") Long userId, @PathParam("pageId") Long pageId, @Context SecurityContext req) {
        return ServiceBeans.getMiddleEarthQuestService().getUserPage(userId, pageId);
    }

    @GET
    @Path("/rest/user/checkout")
    @Produces({MediaType.APPLICATION_JSON})
    public String resetUserPage() {
        return ShopifyService.createCheckout();
    }

    @GET
    @Path("/rest/user/reset/{userId}/{pageId}/{checkoutId}")
    @Produces({MediaType.APPLICATION_JSON})
    public UserPage resetUserPage(@PathParam("userId") Long userId, @PathParam("pageId") Long pageId, @PathParam("checkoutId") String id) {
        String results = ShopifyService.confirmCheckout(id);
        return (results.length() > 350) ? ServiceBeans.getMiddleEarthQuestService().resetUserPage(userId, pageId) : ServiceBeans.getMiddleEarthQuestService().getUserPage(userId, pageId);
    }

    @GET
    @Path("/rest/next/{pageId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<NextPage> getNextPages(@PathParam("pageId") Long pageId) {
        return ServiceBeans.getMiddleEarthQuestService().getNextPages(pageId);
    }

    @GET
    @Path("/rest/page/{parentId}/{pageLabel}/{secret}")
    @Produces({MediaType.TEXT_PLAIN})
    public String addPage(@PathParam("parentId") Long parentId, @PathParam("pageLabel") String pageLabel, @PathParam("secret") String secret) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMiddleEarthQuestService().getPage(parentId, pageLabel);
            return "success";
        }
        return "403";
    }

    @POST
    @Path("/rest/page/{pageId}/{time}/{exp}/{secret}")
    @Produces({MediaType.TEXT_PLAIN})
    public String addPageContent(String content, @PathParam("pageId") Long pageId, @PathParam("time") Long time, @PathParam("exp") Long exp, @PathParam("secret") String secret) {
        if (secret.equals(socialSecret)) {
            Page page = ServiceBeans.getMiddleEarthQuestService().getPage(pageId);
            page.setTime(time);
            page.setExperiencePoints(exp);
            page.setContent(content);
            ServiceBeans.getMiddleEarthQuestService().addPage(page);
            return "success";
        }
        return "403";
    }

    @POST
    @Path("/rest/merp/{secret}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.TEXT_PLAIN)
    public String createMERPCharacter(MERPCharacter character, @PathParam("secret") String secret) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMERPCharacterService().addCharacter(character);
            return character.getId().toString();
        }
        return "403";
    }

    @GET
    @Path("/rest/merp")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MERPCharacter> getMERPCharacters() {
        return ServiceBeans.getMERPCharacterService().getSimpleCharacters();
    }

    @GET
    @Path("/rest/merp/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public MERPCharacter getMERPCharacters(@PathParam("id") Long id) {
        return ServiceBeans.getMERPCharacterService().getMERPCharacter(id);
    }

    @GET
    @Path("/rest/merp/race/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public MERPCharacterRace createMERPCharacterRace(@PathParam("id") Long id) {
        return ServiceBeans.getMERPCharacterService().getMERPCharacterRace(id);
    }

    @GET
    @Path("/rest/merp/homes/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MERPCharacterHome> createMERPCharacterHomes(@PathParam("id") Long id) {
        return ServiceBeans.getMERPCharacterService().getCharacterHomes(id);
    }

    @GET
    @Path("/rest/merp/names/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MERPCharacterName> createMERPCharacterNames(@PathParam("id") Long id) {
        return ServiceBeans.getMERPCharacterService().getCharacterNames(id);
    }

    @GET
    @Path("/rest/merp/{secret}/{id}/{race}")
    @Produces(MediaType.TEXT_PLAIN)
    public String createMERPCharacterRace(@PathParam("secret") String secret, @PathParam("id") Long id, @PathParam("race") String race) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMERPCharacterService().addCharacterRace(id, race);
            return "success";
        }
        return "403";
    }

    @POST
    @Path("/rest/merp/homes/{secret}/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.TEXT_PLAIN)
    public String createMERPCharacterHomes(List<String> homes, @PathParam("secret") String secret, @PathParam("id") Long id) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMERPCharacterService().addCharacterHomes(id, homes);
            return "success";
        }
        return "403";
    }

    @POST
    @Path("/rest/merp/names/{secret}/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.TEXT_PLAIN)
    public String createMERPCharacterNames(List<String> names, @PathParam("secret") String secret, @PathParam("id") Long id) {
        if (secret.equals(socialSecret)) {
            ServiceBeans.getMERPCharacterService().addCharacterNames(id, names);
            return "success";
        }
        return "403";
    }

    @Override
    public void run() {
        setProcess(getClass().getCanonicalName());
        setOrigin("http://localhost/");
        setPort(13370);
        super.run();
    }

    public static void main(String... args) {
        new Thread(new MessageRestService()).start();
    }
}
