/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.entity.dao.service;

import ass.ass.inate.entity.User;
import ass.ass.inate.entity.dao.UserDAO;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nathan Keene
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDao;

    @Transactional
    @Override
    public User add(User user) {
        return userDao.add(user);
    }
    
    @Transactional
    @Override
    public User get(Long userId) {
        return userDao.get(userId);
    }

    @Transactional
    @Override
    public List<User> listUsers() {
        return userDao.listUsers();
    }

}
