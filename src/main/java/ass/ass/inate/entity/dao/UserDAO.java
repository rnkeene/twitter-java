/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.entity.dao;

import ass.ass.inate.entity.User;
import java.util.List;

/**
 *
 * @author rnkee
 */
public interface UserDAO {

    User add(User user);

    User get(Long user);
    
    List<User> listUsers();
}
