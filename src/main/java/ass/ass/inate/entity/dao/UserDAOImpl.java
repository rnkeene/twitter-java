/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.entity.dao;

import ass.ass.inate.entity.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nathan Keene
 */
@Repository
public class UserDAOImpl implements UserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User add(User user) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> from = criteriaQuery.from(User.class);
        criteriaQuery.where(criteriaBuilder.equal(from.get("googleId"), user.getGoogleId()));
        User collision = user;
        try {
            collision = entityManager.createQuery(criteriaQuery).getSingleResult();            
            collision.setEmail(user.getEmail());
            collision.setFamilyName(user.getFamilyName());
            collision.setFullName(user.getFullName());
            collision.setGivenName(user.getGivenName());
            collision.setImageURL(user.getImageURL());
            entityManager.persist(collision);
            
        } catch (NoResultException e) {
            entityManager.persist(collision);
        }
        entityManager.close();
        return collision;
    }
    
    @Override
    public User get(Long userId) {
        User user = entityManager.find(User.class, userId);
        entityManager.close();
        return user;
    }

    @Override
    public List<User> listUsers() {
        CriteriaQuery<User> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(User.class);
        criteriaQuery.from(User.class);
        List<User> users = entityManager.createQuery(criteriaQuery).getResultList();
        entityManager.close();
        return users;
    }

}
