/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.entity;

import ass.ass.inate.json.JsonUtil;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author Nathan Keene
 */
@MappedSuperclass
public class Identity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Identity() {
    }

    public Identity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return JsonUtil.toJSON(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Identity local = (Identity) (obj);

        if (this.id == null ? local.id != null : !this.id.equals(local.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        if(id==null){
            return 0;
        }
        return id.hashCode();
    }

}
