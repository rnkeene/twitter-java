/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.shopify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author Nathan Keene
 */
public class ShopifyService {

    private static final String DOMAIN = "https://keenedom.myshopify.com";
    private static final String GRAPHQL = DOMAIN + "/api/graphql";
    private static final String STOREFRONT_API = "86ee0b08a1a5a38ee7e7f73af809b1dd";

    public static String createCheckout() {
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(GRAPHQL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/graphql");
            conn.setRequestProperty("X-Shopify-Storefront-Access-Token", STOREFRONT_API);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            String outString = "mutation {\n"
                    + "  checkoutCreate(input: {\n"
                    + "    lineItems: [{ variantId: \"Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8xMjI0MzI1NjM0NDY4MA==\", quantity: 1 }]\n"
                    + "  }) {\n"
                    + "    checkout {\n"
                    + "       id\n"
                    + "       webUrl\n"
                    + "       completedAt\n"
                    + "       lineItems(first: 5) {\n"
                    + "         edges {\n"
                    + "           node {\n"
                    + "             title\n"
                    + "             quantity\n"
                    + "           }\n"
                    + "         }\n"
                    + "       }\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";
            System.out.println(outString);
            byte[] out = outString.getBytes(StandardCharsets.UTF_8);
            int length = out.length;
            conn.setFixedLengthStreamingMode(length);
            conn.connect();
            try (OutputStream os = conn.getOutputStream()) {
                os.write(out);
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
        return result.toString();
    }

    public static String confirmCheckout(String id) {
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(GRAPHQL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/graphql");
            conn.setRequestProperty("X-Shopify-Storefront-Access-Token", STOREFRONT_API);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            String outString = "query {\n"
                    + "  node(id:\"" + id + "\" ) {\n"
                    + "    ... on Checkout {\n"
                    + "      id\n"
                    + "      webUrl\n"
                    + "      completedAt\n"
                    + "      orderStatusUrl\n"
                    + "      order{\n"
                    + "        id\n"
                    + "      }\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";
            System.out.println(outString);
            byte[] out = outString.getBytes(StandardCharsets.UTF_8);
            int length = out.length;
            conn.setFixedLengthStreamingMode(length);
            conn.connect();
            try (OutputStream os = conn.getOutputStream()) {
                os.write(out);
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
        return result.toString();
    }
}
