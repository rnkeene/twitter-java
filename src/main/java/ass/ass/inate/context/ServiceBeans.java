/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.ass.inate.context;

import ass.ass.inate.entity.DatabaseConfiguration;
import ass.ass.inate.entity.dao.service.UserService;
import ass.ass.inate.merp.entity.dao.service.MERPCharacterService;
import ass.ass.inate.merp.entity.dao.service.MiddleEarthQuestService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author rnkee
 */
public class ServiceBeans {
        
    private static final AnnotationConfigApplicationContext CONTEXT = new AnnotationConfigApplicationContext(DatabaseConfiguration.class);

    private static final UserService USER_SERVICE = CONTEXT.getBean(UserService.class);
    
    private static final MiddleEarthQuestService MIDDLE_EARTH_QUEST_SERVICE = CONTEXT.getBean(MiddleEarthQuestService.class);

    private static final MERPCharacterService MERP_CHARACTER_SERVICE = CONTEXT.getBean(MERPCharacterService.class);
    
    public static UserService getUserService() {
        return USER_SERVICE;
    }

    public static MiddleEarthQuestService getMiddleEarthQuestService() {
        return MIDDLE_EARTH_QUEST_SERVICE;
    }
    
    public static MERPCharacterService getMERPCharacterService() {
        return MERP_CHARACTER_SERVICE;
    }
 
    public ServiceBeans(){}   
}
