define(["./div", "./style"], function(divBuilder, style) {
    var saved, linkSelected = style("linkSelected");
    linkSelected("font-weight", "900");
    linkSelected.promote();
    return function(named, x, y, width, height) {
        var menu = divBuilder(named, x, y, width, height);
        return function(label, instance, parent, callback) {        
            var link = document.createElement("div"),
                keep = callback,
                toggle = function() {
                    if (saved){
                        saved();
                        saved = null;
                    }
                    linkSelected.toggle(link, false);
                    parent.appendChild(instance.div);
                    if(keep){
                        keep();
                    }
                    var backup = toggle;
                    toggle = function() {
                        linkSelected.toggle(link, true);
                        if(parent.contains(instance.div)){
                            parent.removeChild(instance.div);
                        }
                        toggle = backup;
                    }
                    saved = toggle;
                }
            
            link.addEventListener("click", function(event) {
                toggle();
            }, true);
            link.appendChild(document.createTextNode(label));
            menu.div.appendChild(link);
            return menu
        }
    }
});