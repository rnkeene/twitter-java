requirejs.config({
    baseUrl: 'js'
});
var loginEvent = document.createEvent("Event"),    
    postUser = function(userProfile) {
        require(["./ajax"], function(ajax) {
            var service = ajax(function(response) {
                user = JSON.parse(response.response);
                document.dispatchEvent(loginEvent);
            });
            service.init("POST", '/rest/user');
            service.header('Content-Type', 'application/json;charset=UTF-8');
            service.send(JSON.stringify(userProfile));
        });
    },
    loaded = true,
    onSignIn = function(googleUser) {
        var profile = googleUser.getBasicProfile(),
            sendUserProfile = function() {
                if (typeof postUser === "undefined") {
                    window.setTimeout(function() {
                        sendUserProfile();
                    }, 0);
                } else {
                    postUser({
                        fullName: profile.getName(),
                        givenName: profile.getGivenName(),
                        familyName: profile.getFamilyName(),
                        email: profile.getEmail(),
                        imageURL: profile.getImageUrl(),
                        googleId: profile.getId()
                    })
                }
            }
        sendUserProfile();
    };

loginEvent.initEvent('userLogin', true, true);
window.addEventListener("load", function() {
    require(["./canvas/play"], function(play) {
        document.body.appendChild(play.element);
    });
    /*
    require(["./menu/googleCharts"], function(instance) {
        parent.appendChild((instance(parent)).div);
    });
    */
    require(["./book/graph"], function(graph) {
        var html = graph();
        parent.appendChild(html.div);
    });
}, true);

(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-46131768-5', 'auto');
ga('send', 'pageview');