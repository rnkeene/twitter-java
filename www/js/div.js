define(["./style"], function(style) {
    var divHistory = [
            [],
            []
        ],
        dragAndDrop = function(div, named) {
            var index = divHistory[0].indexOf(named);
            if (index > -1) {
                divHistory[0].slice(index, index + 1);
                divHistory[1].slice(index, index + 1);
                divHistory[0].push(named);
                divHistory[1].push(div);
            } else {
                divHistory[0].push(named);
                divHistory[1].push(div);
            }
            divHistory[0].forEach(function(name, index, list) {
                var zIndex = style(name);
                zIndex("z-index", index);
                zIndex.promote();
            });
        }
    return function(named, x, y, initWidth, initHeight) {
        var top = y,
            left = x,
            width = initWidth,
            height = initHeight,
            div = document.createElement("div"),
            divStyle = style(named),
            windowX = window.innerWidth,
            windowY = window.innerHeight,
            clientX, clientY, deltaX, deltaY,
            resizeDirection, toggleState = false,
            reset, moving = false,
            direction = function() {
                divWidth = div.offsetWidth,
                    divHeight = div.offsetHeight;
                if (top < 0) {
                    top = 0;
                } else if (top + divHeight > windowY) {
                    top = windowY - divHeight;
                }
                if (left < 0) {
                    left = 0;
                } else if (left + divWidth > windowX) {
                    left = windowX - divWidth;
                }
                divStyle("top", top + "px");
                divStyle("left", left + "px");
                divStyle.promote();
            },
            divMove = function(event) {
                deltaX = event.clientX - clientX,
                    deltaY = event.clientY - clientY;
                clientX = event.clientX,
                    clientY = event.clientY;
                top = top + deltaY,
                    left = left + deltaX;
                direction();
            },
            divDown = function(event) {
                clientX = event.clientX,
                    clientY = event.clientY;
                divStyle("cursor", "move");
                divStyle.promote();
                moving = true;
                dragAndDrop(div, named);
                div.removeEventListener("mousedown", divDown, true);
                div.addEventListener("mouseup", divUp, true);
                div.addEventListener("mousemove", divMove, true);
            },
            toggle = function(on) {
                toggleState = on;
                if (on) {
                    div.removeEventListener("mousedown", divDown, true);
                    div.removeEventListener("mouseover", initDiv, true);
                    div.removeEventListener("mouseout", quitDiv, true);
                } else {
                    div.addEventListener("mousedown", divDown, true)
                    div.addEventListener("mouseover", initDiv, true);
                    div.addEventListener("mouseout", quitDiv, true);
                }
            },
            dimensions = function() {
                var divWidth = div.offsetWidth,
                    divHeight = div.offsetHeight;
                return {
                    width: divWidth,
                    height: divHeight
                }
            },
            divUp = function(event) {
                divStyle("cursor", "auto");
                divStyle.promote();
                moving = false;
                div.removeEventListener("mouseup", divUp, true);
                div.removeEventListener("mousemove", divMove, true);
                div.addEventListener("mousedown", divDown, true);
            },
            namedResize = document.createEvent("Event"),
            divResizeMove = function(event) {
                divWidth = div.offsetWidth,
                    divHeight = div.offsetHeight;
                switch (resizeDirection) {
                    case 'nw':
                        width = width + (left - event.clientX),
                            height = height + (top - event.clientY);
                        left = event.clientX,
                            top = event.clientY;
                        divStyle("top", top + "px");
                        divStyle("left", left + "px");
                        divStyle("height", height + "px");
                        divStyle("width", width + "px");
                        divStyle.promote();
                        break;
                    case 'sw':
                        width = width + (left - event.clientX),
                            height = height + (event.clientY - divHeight - top);
                        left = event.clientX;
                        divStyle("left", left + "px");
                        divStyle("height", height + "px");
                        divStyle("width", width + "px");
                        divStyle.promote();
                        break;
                    case 'e':
                        width = width + (left - event.clientX);
                        left = event.clientX;
                        divStyle("width", width + "px");
                        divStyle("left", left + "px");
                        divStyle.promote();
                        break;
                    case 'ne':
                        width = width + (event.clientX - divWidth - left),
                            height = height + (top - event.clientY);
                        top = event.clientY;
                        divStyle("top", top + "px");
                        divStyle("height", height + "px");
                        divStyle("width", width + "px");
                        divStyle.promote();
                        break;
                    case 'se':
                        width = width + (event.clientX - divWidth - left),
                            height = height + (event.clientY - divHeight - top);
                        divStyle("height", height + "px");
                        divStyle("width", width + "px");
                        divStyle.promote();
                        break;
                    case 'w':
                        width = width + (event.clientX - divWidth - left),
                            divStyle("width", width + "px");
                        divStyle.promote();
                        break;
                    case 'n':
                        height = height + (top - event.clientY),
                            top = event.clientY;
                        divStyle("top", top + "px");
                        divStyle("height", height + "px");
                        divStyle.promote();
                        break;
                    case 's':
                        height = height + (event.clientY - divHeight - top);
                        divStyle("height", height + "px");
                        divStyle.promote();
                        break;
                    default:
                        break;
                }
                document.dispatchEvent(namedResize);
            },
            divResizeUp = function(event) {
                window.removeEventListener("mouseup", divResizeUp, true);
                window.removeEventListener("mousemove", divResizeMove, true);
                div.addEventListener("mouseover", initDiv, true);
                div.addEventListener("mouseout", quitDiv, true);
                div.addEventListener("mousemove", divResize, true);
                div.addEventListener("mousedown", divResizeDown, true);
            },
            divResizeDown = function(event) {
                div.removeEventListener("mouseover", initDiv, true);
                div.removeEventListener("mouseout", quitDiv, true);
                div.removeEventListener("mousedown", divResizeDown, true);
                div.removeEventListener("mousemove", divResize, true);
                window.addEventListener("mouseup", divResizeUp, true);
                window.addEventListener("mousemove", divResizeMove, true);
            },
            divResizeOwner = function(cursor) {
                resizeDirection = cursor;
                div.removeEventListener("mousedown", divDown, true),
                    div.addEventListener("mousedown", divResizeDown, true),
                    divStyle("cursor", cursor + "-resize");
                divStyle.promote();
            },
            divResize = function(event) {
                var divWidth = div.offsetWidth,
                    divHeight = div.offsetHeight,
                    localX = event.clientX,
                    localY = event.clientY,
                    onXleft = false,
                    onYtop = false,
                    onXright = false,
                    onYbottom = false
                if (localX - left < 8) {
                    onXleft = true;
                } else if (localX - divWidth - left > -8) {
                    onXright = true;
                }
                if (localY - top < 8) {
                    onYtop = true;
                } else if (localY - divHeight - top > -8) {
                    onYbottom = true;
                }
                if (onXleft) {
                    if (onYtop) {
                        divResizeOwner("nw");
                    } else if (onYbottom) {
                        divResizeOwner("sw");
                    } else {
                        divResizeOwner("e");
                    }
                } else if (onXright) {
                    if (onYtop) {
                        divResizeOwner("ne");
                    } else if (onYbottom) {
                        divResizeOwner("se");
                    } else {
                        divResizeOwner("w");
                    }
                } else if (onYtop) {
                    divResizeOwner("n");
                } else if (onYbottom) {
                    divResizeOwner("s");
                } else {
                    div.removeEventListener("mousedown", divResizeDown, true);
                    if (!toggleState) {
                        div.addEventListener("mousedown", divDown, true);
                    }
                    if (!moving) {
                        divStyle("cursor", "auto");
                        divStyle.promote();
                    }
                }
            },
            initDiv = function(event) {
                div.addEventListener("mousedown", divDown, true);
                div.addEventListener("mousemove", divResize, true);
            },
            quitDiv = function(event) {
                div.removeEventListener("mousedown", divDown, true);
                div.removeEventListener("mousemove", divResize, true);
            };

        namedResize.initEvent(named+'Resize', true, true);
        var divWidth = div.offsetWidth,
            divHeight = div.offsetHeight;
        div.addEventListener("mouseover", initDiv, true),
            div.addEventListener("mouseout", quitDiv, true);

        divStyle("height", height + "px");
        divStyle("width", width + "px");
        divStyle("background-color", "#FFFFFF");
        divStyle("color", "#000000");
        divStyle("overflow", "hidden");
        divStyle("position", "absolute");
        divStyle("padding", "10px");
        divStyle("top", top + "px");
        divStyle("left", left + "px");
        divStyle.toggle(div);
        divStyle.promote();

        window.addEventListener("resize", function() {
            windowX = window.innerWidth,
                windowY = window.innerHeight;
            direction();
        }, true);

        dragAndDrop(div, named);
        return {
            named: named,
            div: div,
            toggle: toggle,
            dimensions: dimensions,
            bringForward:function(){
                dragAndDrop(div, named);
            }
        }
    }
});