define(["./style"], function(style) {    
    var bodyStyle = style("body"),
         highlight = style('highlight');
    bodyStyle("background-color", "#000000");
    bodyStyle("position", "static");
    bodyStyle("overflow", "hidden");
    bodyStyle("margin", "0px");
    bodyStyle("padding", "0px");
    bodyStyle("color", "#FFFFFF");
    bodyStyle.as.native();
    bodyStyle.promote();
    highlight('cursor', 'pointer');
    highlight('font-weight', '900');
    highlight('text-decoration', 'underline');
    highlight.promote();
    return function(element, wrapper) {
        element.addEventListener('mouseover', function() {
            highlight.toggle(element, false);
            wrapper.toggle(true);
        }, true);
        element.addEventListener('mousedown', function() {
            highlight.toggle(element, true);
            wrapper.toggle(false);
        }, true);
        element.addEventListener('mouseout', function() {
            highlight.toggle(element, true);
            wrapper.toggle(false);
        }, true);
    }
});