define(function() {
    var canvas = document.createElement("canvas");
    canvas.width = window.innerWidth,
        canvas.height = window.innerHeight;
    var context = canvas.getContext("2d");
    
    
    var now, x, y, up = function(event){
            var xb = event.clientX, yb = event.clientY, later = Date.now();
            var delta = later-now;
            var grd = context.createRadialGradient(x, y, 1, xb, yb, 100);
            grd.addColorStop(0, "yellow");
            grd.addColorStop(1, "black");        
            context.fillStyle = grd;
            context.fillRect(0, 0, canvas.width, canvas.height);
            window.removeEventListener("mouseup", up, true);
            canvas.addEventListener("mousedown", down, true);
        }, down = function(event){
            x = event.clientX, y = event.clientY;
            now = Date.now();
            window.removeEventListener("mousedown", down, true);
            window.addEventListener("mouseup", up, true);
        };
    canvas.addEventListener("mousedown", down, true);
    return {
        element: canvas,
        context: context
    }
    
});