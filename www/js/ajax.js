define(function() {
    return function(callback){
        var xhttp = new XMLHttpRequest(); 
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                callback(this);
           }
        };
        return {
            init: function(type, url){
                xhttp.open(type, url, true);
            },
            header: function(header, value){
                xhttp.setRequestHeader(header, value);
            },
            send: function(data){                
                xhttp.send(data);
            }
        }
    }
});