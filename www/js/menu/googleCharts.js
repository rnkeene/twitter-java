define(["../div", "../menu", "../content/sankeyDiagram", "../content/wordTree", "../content/comboChart", "../content/ganttChart", "../content/gaugeChart", "../content/pieChart", "../content/tableChart", "../content/timelineChart", "../content/treeMapChart", "../content/trendlineChart", "../content/barChart", "../content/orgChart"], function(container, menuBuilder, sankey, wordTree, comboChart, ganttChart, gaugeChart, pieChart, tableChart, timelineChart, treeMapChart, trendlineChart, barChart, orgChart) {
    return function(parent) {
        var wordTreeDiv = container("wordTreeDiv", 800, 100, 900, 500),
            sankeyDiv = container("sankeyDiv", 800, 100, 900, 500),
            comboChartDiv = container("comboChartDiv", 800, 100, 900, 500),
            ganttChartDiv = container("ganttChartDiv", 800, 100, 900, 500),
            gaugeChartDiv = container("gaugeChartDiv", 800, 100, 900, 500),
            pieChartDiv = container("pieChartDiv", 800, 100, 900, 500),
            tableChartDiv = container("tableChartDiv", 800, 100, 900, 500),
            timelineChartDiv = container("timelineChartDiv", 800, 100, 900, 500),
            treeMapChartDiv = container("treeMapChartDiv", 800, 100, 900, 500),
            trendlineChartDiv = container("trendlineChartDiv", 800, 100, 900, 500),
            barChartDiv = container("barChartDiv", 800, 100, 900, 500),
            orgChartDiv = container("orgChartDiv", 800, 100, 900, 500),
            menu = menuBuilder("GoogleChartsMenu", 30, 30, 300, 300);
        menu("Sankey Diagram Example", sankeyDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Sankey Diagram Example', 1);
            sankey(sankeyDiv.div);
        });
        menu("Combo Chart Example", comboChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Combo Chart Example', 1);
            comboChart(comboChartDiv.div);
        });
        menu("Gantt Chart Example", ganttChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Gantt Chart Example', 1);
            ganttChart(ganttChartDiv.div);
        });
        menu("Gauge Chart Example", gaugeChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Gauge Chart Example', 1);
            gaugeChart(gaugeChartDiv.div);
        });
        menu("Pie Chart Example", pieChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Pie Chart Example', 1);
            pieChart(pieChartDiv.div);
        });
        menu("Table Chart Example", tableChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Table Chart Example', 1);
            tableChart(tableChartDiv.div);
        });
        menu("Timeline Chart Example", timelineChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Timeline Chart Example', 1);
            timelineChart(timelineChartDiv.div);
        });
        menu("Tree Map Chart Example", treeMapChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Tree Map Chart Example', 1);
            treeMapChart(treeMapChartDiv.div);
        });
        menu("Trendline Chart Example", trendlineChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Trendline Chart Example', 1);
            trendlineChart(trendlineChartDiv.div);
        });
        menu("Bar Chart Example", barChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Bar Chart Example', 1);
            barChart(barChartDiv.div);
        });
        menu("Org Chart Example", orgChartDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Org Chart Example', 1);
            orgChart(orgChartDiv.div);
        });
        var instance = menu("Word Tree Example", wordTreeDiv, parent, function() {
            ga('send', 'event', 'Menu', 'chart', 'Word Tree Example', 1);
            wordTree(wordTreeDiv.div);
        })
        return instance;
    }
});