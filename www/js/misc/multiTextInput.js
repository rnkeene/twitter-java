define(['../misc/textInput'], function(smallInput){
    return function(input, wrapper){
        var alt = [], process=function(){
            var next = smallInput(30, '');
            next.addEventListener('change', function(){
                if(next.value!==''){
                    add();
                }else{
                    var index = alt.indexOf(next);
                    if(index < (alt.length-1)){
                        wrapper.removeChild(next);
                        alt.splice(index, 1);
                    }
                }
            }, true);
            alt.push(next);
            wrapper.appendChild(next);
        }, add =function(){
            if(alt.length){
                if(alt[alt.length-1].value!==''){            
                    process();
                }
            }else{
                process();
            }
        }
        input.addEventListener('change', function(){
            if(input.value!==''){
                add();
            }
        }, true);
        return function(data){
            if(data){
                data.forEach(function(value){
                    var next = smallInput(30, value);
                    next.addEventListener('change', function(){
                        if(next.value!==''){
                            add();
                        }else{
                            var index = alt.indexOf(next);
                            if(index < (alt.length-1)){
                                wrapper.removeChild(next);
                                alt.splice(index, 1);
                            }
                        }
                    }, true);
                    alt.push(next);
                    wrapper.appendChild(next);
                });
            }
            return [].concat(alt);
        }
    }
});