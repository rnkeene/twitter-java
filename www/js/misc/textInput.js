define(function(){
    return function(size, value){
        var input = document.createElement("input");
        input.type='text';
        input.size=size;
        input.value=value;
        return input;
    }
});