define(function() {
    var options, data, 
        drawChart = function() {
            data = google.visualization.arrayToDataTable(
                [
                    ['Phrases'],
                    ['cats are better than dogs'],
                    ['cats eat kibble'],
                    ['cats are better than hamsters'],
                    ['cats are awesome'],
                    ['cats are people too'],
                    ['cats eat mice'],
                    ['cats meowing'],
                    ['cats in the cradle'],
                    ['cats eat mice'],
                    ['cats in the cradle lyrics'],
                    ['cats eat kibble'],
                    ['cats for adoption'],
                    ['cats are family'],
                    ['cats eat mice'],
                    ['cats are better than kittens'],
                    ['cats are evil'],
                    ['cats are weird'],
                    ['cats eat mice'],
                ]
            ), options = {
                wordtree: {
                    format: 'implicit',
                    word: 'cats'
                }
            };

        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.WordTree(div);
        chart.draw(data, options);
    };
});