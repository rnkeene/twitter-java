define(function() {
    var options, data,
        drawChart = function() {
            data = google.visualization.arrayToDataTable([
                    ['Year', 'Sales', 'Expenses', 'Profit'],
                    ['2014', 1000, 400, 200],
                    ['2015', 1170, 460, 250],
                    ['2016', 660, 1120, 300],
                    ['2017', 1030, 540, 350]
                ]),
                options = {
                    chart: {
                        title: 'Company Performance',
                        subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                    },
                    bars: 'horizontal' // Required for Material Bar Charts.
                };
        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.charts.Bar(div);
        chart.draw(data, google.charts.Bar.convertOptions(options));
    };
});