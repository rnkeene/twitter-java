define(function() {
    var options, data,
        drawChart = function() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'ToolTip');

            // For each orgchart box, provide the name, manager, and tooltip to show.
            data.addRows([
                    [{
                            v: 'Mike',
                            f: 'Mike<div style="color:red; font-style:italic">President</div>'
                        },
                        '', 'The President'
                    ],
                    [{
                            v: 'Jim',
                            f: 'Jim<div style="color:red; font-style:italic">Vice President</div>'
                        },
                        'Mike', 'VP'
                    ],
                    ['Alice', 'Mike', ''],
                    ['Bob', 'Jim', 'Bob Sponge'],
                    ['Carol', 'Bob', '']
                ]),
                options = {
                    allowHtml: true
                };

        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.OrgChart(div);
        chart.draw(data, options);
    };
});