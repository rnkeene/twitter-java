define(function() {
    var options, data, chart,
        drawChart = function() {
            data = new google.visualization.DataTable();

            data.addColumn({
                type: 'string',
                id: 'Room'
            });
            data.addColumn({
                type: 'string',
                id: 'Name'
            });
            data.addColumn({
                type: 'date',
                id: 'Start'
            });
            data.addColumn({
                type: 'date',
                id: 'End'
            });
            data.addRows([
                    ['Magnolia Room', 'CSS Fundamentals', new Date(0, 0, 0, 12, 0, 0), new Date(0, 0, 0, 14, 0, 0)],
                    ['Magnolia Room', 'Intro JavaScript', new Date(0, 0, 0, 14, 30, 0), new Date(0, 0, 0, 16, 0, 0)],
                    ['Magnolia Room', 'Advanced JavaScript', new Date(0, 0, 0, 16, 30, 0), new Date(0, 0, 0, 19, 0, 0)],
                    ['Gladiolus Room', 'Intermediate Perl', new Date(0, 0, 0, 12, 30, 0), new Date(0, 0, 0, 14, 0, 0)],
                    ['Gladiolus Room', 'Advanced Perl', new Date(0, 0, 0, 14, 30, 0), new Date(0, 0, 0, 16, 0, 0)],
                    ['Gladiolus Room', 'Applied Perl', new Date(0, 0, 0, 16, 30, 0), new Date(0, 0, 0, 18, 0, 0)],
                    ['Petunia Room', 'Google Charts', new Date(0, 0, 0, 12, 30, 0), new Date(0, 0, 0, 14, 0, 0)],
                    ['Petunia Room', 'Closure', new Date(0, 0, 0, 14, 30, 0), new Date(0, 0, 0, 16, 0, 0)],
                    ['Petunia Room', 'App Engine', new Date(0, 0, 0, 16, 30, 0), new Date(0, 0, 0, 18, 30, 0)]
                ]), options = {
                    timeline: {
                        colorByRowLabel: true
                    },
                    backgroundColor: '#ffd'
                };
        }
    loadChart = function() {
        if (typeof data === "undefined") {
            window.setTimeout(function() {
                loadChart()
            }, 0);
        }
    };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        chart = new google.visualization.Timeline(div);
        chart.draw(data, options);
    };
});