define(function() {    
    google.charts.load('current', {packages: ['table','corechart','gantt', 'sankey', 'wordtree', 'gauge', 'timeline', 'treemap','bar','orgchart']});
    var data, options, drawChart = function() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'From');
            data.addColumn('string', 'To');
            data.addColumn('number', 'Weight');
            data.addRows([
                ['A', 'X', 5],
                ['A', 'Y', 7],
                ['A', 'Z', 6],
                ['B', 'X', 2],
                ['B', 'Y', 9],
                ['B', 'Z', 4]
            ]), options = {
                width: 600,
            };
        },
        loadChart = function() {
            if (typeof data === "undefined" ) {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.Sankey(div);
        chart.draw(data, options);
    };
});