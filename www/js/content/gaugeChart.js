define(function() {
    var options, data, chart,
        drawChart = function() {
            data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Memory', 80],
                ['CPU', 55],
                ['Network', 68]
            ]),options = {
                width: 400,
                height: 120,
                redFrom: 90,
                redTo: 100,
                yellowFrom: 75,
                yellowTo: 90,
                minorTicks: 5
            };
        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        chart = new google.visualization.Gauge(div);
        chart.draw(data, options);
    };
});