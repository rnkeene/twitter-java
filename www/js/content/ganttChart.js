define(function() {

    var toMilliseconds = function(minutes) {
            return minutes * 60 * 1000;
        },
        options, data,
        drawChart = function() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('string', 'Resource');
            data.addColumn('date', 'Start');
            data.addColumn('date', 'End');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');

            data.addRows([
                ['toTrain', 'Walk to train stop', 'walk', null, null, toMilliseconds(5), 100, null],
                ['music', 'Listen to music', 'music', null, null, toMilliseconds(70), 100, null],
                ['wait', 'Wait for train', 'wait', null, null, toMilliseconds(10), 100, 'toTrain'],
                ['train', 'Train ride', 'train', null, null, toMilliseconds(45), 75, 'wait'],
                ['toWork', 'Walk to work', 'walk', null, null, toMilliseconds(10), 0, 'train'],
                ['work', 'Sit down at desk', null, null, null, toMilliseconds(2), 0, 'toWork'],

            ])
            options = {
                height: 275,
                gantt: {
                    defaultStartDateMillis: new Date(2015, 3, 28)
                }
            };

        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.Gantt(div);
        chart.draw(data, options);

    };
});