define(function() {
    var options, data,
        drawChart = function() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('number', 'Salary');
            data.addColumn('boolean', 'Full Time Employee');
            data.addRows([
                ['Mike', {
                    v: 10000,
                    f: '$10,000'
                }, true],
                ['Jim', {
                    v: 8000,
                    f: '$8,000'
                }, false],
                ['Alice', {
                    v: 12500,
                    f: '$12,500'
                }, true],
                ['Bob', {
                    v: 7000,
                    f: '$7,000'
                }, true]
            ])
        },
        options = {
            showRowNumber: true,
            width: '100%',
            height: '100%'
        }
    loadChart = function() {
        if (typeof data === "undefined") {
            window.setTimeout(function() {
                loadChart()
            }, 0);
        }
    };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.Table(div);
        chart.draw(data, options);

    };
});