define(['../style', '../ajax'], function(style, ajax) {
    return function(){
        var socialMessageStyle = style("SocialMessage");
            socialMessageStyle("width", "382px");
            socialMessageStyle.promote();
            var socialMessageInputStyle = style("SocialMessage input");
            socialMessageInputStyle("display", "block");
            socialMessageInputStyle("width", "100%");
            socialMessageInputStyle.promote();
            var socialMessageTextareaStyle = style("SocialMessage textarea");
            socialMessageTextareaStyle("resize", "none");
            socialMessageTextareaStyle("width", "100%");
            socialMessageTextareaStyle.promote();
            var socialMessageButtonStyle = style("SocialMessage button");
            socialMessageButtonStyle("display", "block");
            socialMessageButtonStyle("width", "100%");
            socialMessageButtonStyle("margin-left", "3px");
            socialMessageButtonStyle.promote();

            var service = ajax(function(){}),
                div = document.createElement("div"),
                secret = document.createElement("input"),
                message = document.createElement("textarea"),
                submit = document.createElement("button");

            socialMessageStyle.toggle(div);    
            secret.type = "text";        
            message.rows = '15';
            message.cols = '50';
            message.maxLength='280';

            submit.appendChild(document.createTextNode('Post to Social Media'));
            submit.addEventListener('mousedown', function(){
                service.init("GET", '/rest/' + secret.value + "?message=" + encodeURIComponent(message.value));
                service.send();
            }, true);

            div.appendChild(secret);
            div.appendChild(document.createElement('br'));
            div.appendChild(message);
            div.appendChild(document.createElement('br'));
            div.appendChild(submit);

            return div;
    }
});