define(function() {
    var options, data,
        drawChart = function() {
            data = google.visualization.arrayToDataTable([
                    ['Task', 'Hours per Day'],
                    ['Work', 11],
                    ['Eat', 2],
                    ['Commute', 2],
                    ['Watch TV', 2],
                    ['Sleep', 7]
                ]),
                options = {
                    title: 'My Daily Activities',
                    is3D: true,
                };

        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.PieChart(div);
        chart.draw(data, options);
    };
});