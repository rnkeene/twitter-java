define(function() {
    var options, data,
        drawChart = function() {
            data = google.visualization.arrayToDataTable([
                    ['Diameter', 'Age'],
                    [8, 37],
                    [4, 19.5],
                    [11, 52],
                    [4, 22],
                    [3, 16.5],
                    [6.5, 32.8],
                    [14, 72]
                ]),
                options = {
                    title: 'Age of sugar maples vs. trunk diameter, in inches',
                    hAxis: {
                        title: 'Diameter'
                    },
                    vAxis: {
                        title: 'Age'
                    },
                    legend: 'none',
                    trendlines: {
                        0: {}
                    }
                };
        },
        loadChart = function() {
            if (typeof data === "undefined") {
                window.setTimeout(function() {
                    loadChart()
                }, 0);
            }
        };
    google.charts.setOnLoadCallback(drawChart);
    loadChart();
    return function(div) {
        var chart = new google.visualization.ScatterChart(div);
        chart.draw(data, options);

    };
});