define(["../div", "../style", "../hyperlink"], function(container, style, hyperlink) {
    return function(named, x, y){
        var static = container(named, x, y, 500, window.innerHeight-200),
            staticStyle = style(named);
        staticStyle("background-color", "#ebfaeb");
        staticStyle.promote();
        window.addEventListener('resize', function(){
            staticStyle("height", (window.innerHeight-200)+'px');
            staticStyle.promote();
        });
        return function(xref, title, wrapper){
            xref.addEventListener('mousedown', function(){
                static.bringForward();
                parent.appendChild(static.div);
                staticDivStyle("overflow-y", "scroll");
                var height = static.div.offsetHeight;
                staticDivStyle("height", (height - 50) + "px");
                staticDivStyle.promote();
                document.addEventListener(named+'Resize', function(event){
                    height = static.div.offsetHeight;
                    staticDivStyle("height", (height - 50) + "px");
                    staticDivStyle.promote();
                }, true);
            }, true);
            hyperlink(xref, wrapper);
            staticDiv = document.createElement("div");
            var staticDivStyle = style(named+'Div');
            staticDivStyle.toggle(staticDiv);
            window.addEventListener('resize', function(){
                var height = static.div.offsetHeight;
                staticDivStyle("height", (height - 50) + "px");
                staticDivStyle.promote();
            });
            var staticTitle = document.createElement("h2");
            staticTitle.appendChild(document.createTextNode(title));
            var closeStatic = document.createElement("span");
            closeStatic.appendChild(document.createTextNode('Close ' + title));
            closeStatic.appendChild(document.createElement("br"));
            closeStatic.appendChild(document.createElement("br"));
            closeStatic.addEventListener('mousedown', function(){
                parent.removeChild(static.div);
            }, true);
            hyperlink(closeStatic, static);
            staticDiv.appendChild(staticTitle);
            staticDiv.appendChild(closeStatic);
            return { 
                add: function(node){
                    staticDiv.appendChild(node);
                },
                print:function(){
                    static.div.appendChild(staticDiv);
                }
           }
        }
    }
});