define(["../static", "../../style"], function(static, style){
    return function(title, wrapper){
        var link = static('fighting', 100, 100),
            fightingIntro = document.createElement("blockquote");

        var body = link(title, "Fighting", wrapper);
        fightingIntro.appendChild(document.createTextNode('Fighting, consists of a series of "rounds."  During each "round," you attack your opponent or you attempt to flee and your opponent attacks you.'));
        fightingIntro.appendChild(document.createElement('br'));
        fightingIntro.appendChild(document.createElement('br'));
        fightingIntro.appendChild(document.createTextNode('If you choose to fight an opponent or the text indicates that you "must fight," the combat is resolved in the following fashion:'));
        
        var fightingList = document.createElement("ol"),
            fightListItem = function(content){
                var item = document.createElement("li");
                item.appendChild(document.createTextNode(content));
                fightingList.appendChild(item);
            }
        fightListItem('If you are surprised, proceed directly to step 4; otherwise proceed to step 2.');
        fightListItem('You may make a missile attack if able (see the attack resolution explation).  If your opponent is not surprised (i.e., he is aware of you), he will then make a missile attack against you if able (the text will specifiy if your opponent can make a missile attack).');
        fightListItem('No one is surprised for the remainder of the combat.  Your opponent will attempt to force melee (hand-to-hand combat).  If you wish to continue making missile attacks, pick a number and add your Running bonus.  If the result is 10 or greater, proceed to step 2; otherwise, proceed to step 4.');
        fightListItem('You are engaged in melee.  You make a melee attack against your opponent, and he makes a melee attack against you.  If you are surprised, the order of the attacks is reversed.');
        fightListItem('Repeat rounds of the fight until one of the following conditions occur:');
        fightingIntro.appendChild(fightingList);
        
        var fightingListB = document.createElement("ol"),
            fightListItemB = function(content){
                var item = document.createElement("li");
                item.appendChild(document.createTextNode(content));
                fightingListB.appendChild(item);
            }
        fightListItemB('One of you is killed (a "K" result on the Combat Table); or');
        fightListItemB('One of you has more Damage Taken than Endurance.  That combatant is unconscious and is defeated. (This can also occur due to a "U" result on the Combat Table.); or');
        fightListItemB('You successfully disengage.  At the beginning of any round of combat, you may elect not to attack for that round.  After your opponent makes his attack for that round, you may pick a number and add your Running bonus:');
        fightingList.appendChild(fightingListB);
        
        var fightingListC = document.createElement("ul"),
            fightListItemC = function(content){
                var item = document.createElement("li");
                item.appendChild(document.createTextNode(content));
                fightingListC.appendChild(item);
            }
        fightListItemC('if the result is 9 or greater, you successfully Run Away (follow text instructions).');
        fightListItemC('Otherwise, you are still engaged and must begin another round of the fight at step 4. (However, you may attempt to disengage again)');
        fightingListB.appendChild(fightingListC);
        
        fightingIntro.appendChild(document.createTextNode('Resolve individual attacks as indiciated in the Basic System: Using the Combat Table, cross-index the difference in OB and DB with a number picked.  Use your character\'s Missile OB for a missile attack and Melee OB for a melee attack'));
        
        fightingIntro.appendChild(document.createElement('br'));
        fightingIntro.appendChild(document.createElement('br'));
        
        fightingIntro.appendChild(document.createTextNode('Dice Roll with (Attacker\'s OB - Defender\'s DB)*'));
        
        
        var combatTable = document.createElement("table"),
            combatData = function(numberPickedX, ax, bx, cx, dx, ex, fx, gx, hx, ix, jx){
                topicRow = document.createElement('tr');
                numberPicked = document.createElement('td'),
                a = document.createElement('td'),
                b = document.createElement('td'),
                c = document.createElement('td'),
                d = document.createElement('td'),
                e = document.createElement('td'),
                f = document.createElement('td'),
                g = document.createElement('td'),
                h = document.createElement('td'),
                i = document.createElement('td'),
                j = document.createElement('td');
                numberPicked.appendChild(document.createTextNode(numberPickedX));
                a.appendChild(document.createTextNode(ax));
                b.appendChild(document.createTextNode(bx));
                c.appendChild(document.createTextNode(cx));
                d.appendChild(document.createTextNode(dx));
                e.appendChild(document.createTextNode(ex));
                f.appendChild(document.createTextNode(fx));
                g.appendChild(document.createTextNode(gx));
                h.appendChild(document.createTextNode(hx));
                i.appendChild(document.createTextNode(ix));
                j.appendChild(document.createTextNode(jx));
                topicRow.appendChild(numberPicked);
                topicRow.appendChild(a);
                topicRow.appendChild(b);
                topicRow.appendChild(c);
                topicRow.appendChild(d);
                topicRow.appendChild(e);
                topicRow.appendChild(f);
                topicRow.appendChild(g);
                topicRow.appendChild(h);
                topicRow.appendChild(i);
                topicRow.appendChild(j);
                combatTable.appendChild(topicRow);
            }
        var topicRow = document.createElement('tr');
        var numberPicked = document.createElement('th'),
            a = document.createElement('th'),
            b = document.createElement('th'),
            c = document.createElement('th'),
            d = document.createElement('th'),
            e = document.createElement('th'),
            f = document.createElement('th'),
            g = document.createElement('th'),
            h = document.createElement('th'),
            i = document.createElement('th'),
            j = document.createElement('th');
        numberPicked.appendChild(document.createTextNode('Number Picked'));
        
        a.appendChild(document.createTextNode('+5'));
        b.appendChild(document.createTextNode('+4'));
        c.appendChild(document.createTextNode('+3'));
        d.appendChild(document.createTextNode('+2'));
        e.appendChild(document.createTextNode('+1'));
        f.appendChild(document.createTextNode('0'));
        g.appendChild(document.createTextNode('-1'));
        h.appendChild(document.createTextNode('-2'));
        i.appendChild(document.createTextNode('-3'));
        j.appendChild(document.createTextNode('-4'));
        topicRow.appendChild(numberPicked);
        topicRow.appendChild(a);
        topicRow.appendChild(b);
        topicRow.appendChild(c);
        topicRow.appendChild(d);
        topicRow.appendChild(e);
        topicRow.appendChild(f);
        topicRow.appendChild(g);
        topicRow.appendChild(h);
        topicRow.appendChild(i);
        topicRow.appendChild(j);
        
        combatTable.appendChild(topicRow);
        
        combatData('2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        combatData('3', 2, 1, 1, 1, 0, 0, 0, 0, 0, 0);
        combatData('4', 4, 3, 2, 2, 1, 0, 0, 0, 0, 0);
        combatData('5', 6, 5, 4, 3, 2, 1, 0, 0, 0, 0);
        combatData('6', 7, 6, 5, 4, 3, 2, 1, 0, 0, 0);
        combatData('7', 8, 7, 6, 5, 4, 3, 2, 1, 1, 0);
        combatData('8', 9, 8, 7, 6, 5, 4, 3, 2, 2, 1);
        combatData('9', 'U', 9, 8, 7, 6, 5, 4, 3, 2, 2);
        combatData('10', 'U', 'U', 'U', 8, 7, 7, 6, 5, 4, 3);
        combatData('11', 'K', 'K', 'U', 'U', 'U', 'U', 8, 7, 6, 5);
        combatData('12', 'K', 'K', 'K', 'K', 'K', 'K', 'U', 'U', 'U', 'U');
        
        var combatTableStyle = style('combatTable');
        combatTableStyle.toggle(combatTable);
        combatTableStyle('border', '1px solid black');
        combatTableStyle('border-collapse', 'collapse');        
        combatTableStyle('text-align', 'center');
        combatTableStyle.promote();
        var combatTableTHStyle = style('combatTable th');
        combatTableTHStyle('border', '1px solid black');
        combatTableTHStyle.promote();
        var combatTableCellStyle = style('combatTable td');
        combatTableCellStyle('border', '1px solid black');
        combatTableCellStyle.promote();        
        
        var details = document.createElement('blockquote');
        details.appendChild(document.createTextNode('* - If OB-DB difference is greater than +5, add the excess to the number picked; if the difference is less than -4 treat it as -4.'))
        details.appendChild(document.createElement('br'));
        details.appendChild(document.createElement('br'));
        details.appendChild(document.createTextNode('# - A number result indiciates the amount of damage taken; if the total damage taken exceeds the combatant\'s endurance point total, the character is unconscious.'));
        details.appendChild(document.createElement('br'));
        details.appendChild(document.createTextNode('U - Unconscious (knocked out), wounded, and out of action; see text for results.'));
        details.appendChild(document.createElement('br'));
        details.appendChild(document.createTextNode('K - Killed; if this result is achieved against you your quest is over!'));
        
        body.add(fightingIntro);
        body.add(combatTable);
        body.add(details);
        body.print();
    }
});