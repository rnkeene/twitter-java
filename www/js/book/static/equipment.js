define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('creatingCharacterEquipment', 100, 100),
            createSkillsDetailsIntro = document.createElement("blockquote");
        var body = link(title, "Equipment", wrapper);
        createSkillsDetailsIntro.appendChild(document.createTextNode('You may only wear: 1 suit of armor, 1 dagger (on belt), 1 cloak, 1 backpack, and 1 belt and puch.'));
        createSkillsDetailsIntro.appendChild(document.createElement('br'));
        createSkillsDetailsIntro.appendChild(document.createElement('br'));
        createSkillsDetailsIntro.appendChild(document.createTextNode('In addition, you may carry a number of pieces of equipment equal to your Strength stat; this total may include a maximum of 3 weapons.  If you lose your backpack, this number is reduced by half (round down), and you lose any excess equipment along with the backpack.'));
        createSkillsDetailsIntro.appendChild(document.createElement('br'));
        createSkillsDetailsIntro.appendChild(document.createElement('br'));
        createSkillsDetailsIntro.appendChild(document.createTextNode('Certain special items indiciated by the text may be obtained that do not follow these restrictions'));
        
        var listItem = document.createElement('ul'),
            weapons = document.createElement('li');
        weapons.appendChild(document.createTextNode('Weapons:  if you damage an opponent, your weapon can provide additional damage (this additional damage applies to each attack only when a damage result of 1 or more is obtained):'));
        var weaponsDetails = document.createElement('blockquote');
        weaponsDetails.appendChild(document.createTextNode('Sword ................ +1'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Mace ................. +2 (only if opponent is wearing chain or plate armor)'));
        weaponsDetails.appendChild(document.createElement('br'));        
        weaponsDetails.appendChild(document.createTextNode('Spear ................ +0'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Dagger ............... -1'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Warhammer ............ +2 (but -1 to melee OB)'));
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Battle-axe ........... +2'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Quarterstaff ......... +1'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Two-hand Sword ....... +3 (but -1 to melee OB)'))
        weaponsDetails.appendChild(document.createElement('br'));
        weaponsDetails.appendChild(document.createTextNode('Bare-handed .......... -3 (and -2 to melee OB)'))
        weaponsDetails.appendChild(document.createElement('br'));
        
        var weaponExample = document.createElement('blockquote');
        weaponExample.appendChild(document.createTextNode('EXAMPLE: Using the Combat Table, you inflict "8" damage on your opponent.  If you are using a sword (+1 to damage), your opponent actually takes 9 damage point ("8" + 1).  If you are using a Dagger (-1 to damage), he would take 7 damage points ("8" - 1).'));
        
        weapons.appendChild(weaponsDetails);
        listItem.append(weapons);
        
        var listThrown = document.createElement('ul'),
            thrownWeapons = document.createElement('li');
        thrownWeapons.appendChild(document.createTextNode('Thrown Weapons: The following weapons may be used once in a given combat as a missile attack, missile OB modifications are given in parenthese: spear(-1), dagger(-1), warhammer(-2), sword(-3), mace(-3), battle-axe(-4).  In such a case the weapon may not be used in melee and may only be recovered if you defeat your opponent.'))
        listThrown.appendChild(thrownWeapons);
        
        var bow = document.createElement('ul'),
            bowItem = document.createElement('li');
        bowItem.appendChild(document.createTextNode('A bow may only be used in missile combat (see step 1 under Fighting), not in melee combat.'));
        bow.appendChild(bowItem);
        
        var armor = document.createElement('ul'),
            armorItem = document.createElement('li');
        armorItem.appendChild(document.createTextNode('Armour has the following effects on your skill bonuses:'));
        var armorItemQuote = document.createElement('blockquote');
        
        armorItemQuote.appendChild(document.createTextNode('Plate Armor: +3 to DB; -3 to Trickery, Running and Magical bonuses'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createTextNode('Chain Armor: +2 to DB; -2 to Trickery, Running and Magical bonuses'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createTextNode('Leather Armor: +1 to DB; -1 to Trickery and Running bonuses'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createElement('br'));
        armorItemQuote.appendChild(document.createTextNode('Shield: +1 to DB; -1 to Magical bonus'));
        armorItem.appendChild(armorItemQuote);
        armor.appendChild(armorItem);
        
        var shield = document.createElement('ul'),
            shieldItem = document.createElement('li');
        shieldItem.appendChild(document.createTextNode('A shield may not be used in combination with the following weapons: bow, battle-axe, quarterstaff, or two-hand sword.'));
        shield.appendChild(shieldItem);
        
        body.add(createSkillsDetailsIntro);
        body.add(listItem);
        body.add(weaponsDetails);
        body.add(weaponExample);
        body.add(listThrown);
        body.add(bow);
        body.add(armor);        
        body.add(shield);        
        body.print();
    }
});