define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('creatingCharacterStats', 100, 100),
            createStatsDetailsIntro = document.createElement("blockquote");

        var body = link(title, "Stats", wrapper);
        createStatsDetailsIntro.appendChild(document.createTextNode('  Your character starts with certain mental and physical attributes called "stats" (short for statistics): Strength (St), Agility (Ag), and Intelligence (In).  Before beginning this adventure, determine the values of these stats.  Pick a number 3 times, assign one to each of the three stats, and record them in the Stat Value column of your Character Record.'));
        var createStatsBonuses = document.createElement("h3");
        createStatsBonuses.appendChild(document.createTextNode("Stat Bonuses"));
        var statBonusesIntro = document.createElement("blockquote");
        statBonusesIntro.appendChild(document.createTextNode('Each stat (St, Ag, In) may give a "bonus" when performing certain activities; keep in mind that these "bonuses" can be negative (or zero) as well as positive'));
        statBonusesBlockquote = document.createElement("blockquote");
        statBonusesBlockquote.appendChild(document.createTextNode('Each stat of 2-4 gives a bonus of -1'));
        statBonusesBlockquote.appendChild(document.createElement("br"));
        statBonusesBlockquote.appendChild(document.createTextNode('Each stat of 5-8 gives a bonus of +0 (i.e., no bonus)'));
        statBonusesBlockquote.appendChild(document.createElement("br"));
        statBonusesBlockquote.appendChild(document.createTextNode('Each stat of 9-10 gives a bonus of +1'));
        statBonusesBlockquote.appendChild(document.createElement("br"));
        statBonusesBlockquote.appendChild(document.createTextNode('Each stat of 11-12 gives a bonus of +2'));
        statBonusesBlockquote.appendChild(document.createElement("br"));
        var statBonusesDetails = document.createElement("blockquote");
        statBonusesDetails.appendChild(document.createTextNode('  Record these bonuses in the Stat Bonus column next to the Stat Values on your Character Record.'));
        var applyingStatsBonuses = document.createElement("h3");
        applyingStatsBonuses.appendChild(document.createTextNode('Applying Stat Bonuses to Skills'));
        var applyingStatsBonusesDetails = document.createElement("blockquote");
        applyingStatsBonusesDetails.appendChild(document.createTextNode('  In the Skill section on your character record there is also a Stat Bonus column.  Each space has a stat abbreviation next to it; in each space record the stat bonus corresponding to the abbreviation.'));
        var totalBonusesTitle = document.createElement("h2");
        totalBonusesTitle.appendChild(document.createTextNode('Total Bonuses'));
        var totalBonusesDetails = document.createElement("blockquote");
        totalBonusesDetails.appendChild(document.createTextNode('  At this point, you should have a bonus recorded in each Stat Bonus space and each Skill Bonus space; keep in mind that these "bonuses" can be negative as well as positive.  For each skill, add the two bonuses and record the total in the appropriate Total Bonus space.'));
        totalBonusesDetails.appendChild(document.createElement("br"));
        totalBonusesDetails.appendChild(document.createElement("br"));
        totalBonusesDetails.appendChild(document.createTextNode('  When the text instructs you to "add your bonus," it is referring to these Total Bonuses.'));
        totalBonusesDetails.appendChild(document.createElement("br"));
        totalBonusesDetails.appendChild(document.createElement("br"));
        totalBonusesDetails.appendChild(document.createTextNode('During play, you may acquire equipment or abilities that may effect your bonuses.  The Special Bonus space may be used to record these bonuses.  The Special Bonus space may be used to record these bonuses; of course, some of the Total Bonuses will have to be recalculated when this occurs.'));
        var enduranceTitle = document.createElement("h2");
        enduranceTitle.appendChild(document.createTextNode('Endurance'));
        var enduranceDetails = document.createElement('span');
        enduranceDetails.appendChild(document.createTextNode('  Your Strength stat determines the Endurance of your character.  During combat you will take damage due to shock, pain, bleeding, etc.  If this "Damage Taken" exceeds your Endurance you will fall unconscious (pass out).  Your Endurance is equal to:'));
        enduranceDetails.appendChild(document.createElement("br"));
        var enduranceDetailsCalc = document.createElement("blockquote");
        enduranceDetailsCalc.appendChild(document.createTextNode('20 plus twice your Strength stat.'));
        enduranceDetails.appendChild(enduranceDetailsCalc);
        enduranceDetails.appendChild(document.createTextNode('  Record this on your Character Record'));
        body.add(createStatsDetailsIntro);
        body.add(createStatsBonuses);
        body.add(statBonusesIntro);
        body.add(statBonusesBlockquote);
        body.add(statBonusesDetails);
        body.add(applyingStatsBonuses);
        body.add(applyingStatsBonusesDetails);
        body.add(totalBonusesTitle);
        body.add(totalBonusesDetails);
        body.add(enduranceTitle);
        body.add(enduranceDetails);
        body.print();
        
    }
});