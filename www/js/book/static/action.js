define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('takingAction', 100, 100),
            takingActionIntro = document.createElement("blockquote");

        var body = link(title, "Taking an Action", wrapper);
        takingActionIntro.appendChild(document.createTextNode('Choose one of the actions listed and follow the directions given.  Sometimes these directions will require you to pick a number and use the "Total Bonuses" listed on the Character Record.'));
        
        var actionList = document.createElement('ul'),
            actionListItem = function(content){
                var item = document.createElement("li");
                item.appendChild(document.createTextNode(content));
                actionList.appendChild(item);
            };
        actionListItem('Attack: you must fight your opponent');
        actionListItem('Run Away: Pick a number and add your Running bonus.  If the result is 8 or greater, follow the text instructions.  Otherwise you must fight your opponent and you are "surprised" (i.e., he gets to attack first)');
        
        takingActionIntro.appendChild(actionList);
        
        body.add(takingActionIntro);
        body.print();
    }
});