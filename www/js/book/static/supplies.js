define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('startingSupplies', 100, 100),
            startingSuppliesIntro = document.createElement("blockquote");

        var body = link(title, "Starting Supplies", wrapper);
        startingSuppliesIntro.appendChild(document.createTextNode('You may begin with a spear, shield and/or leather armor, unless you are playing a character who has already been developed through previous adventures in this series.  In that case, you may begin with his/her familiar equipment.  Regardless, you may also have two (2) doses of healing herb which, when ingested at any time during play, will reduce your Damage Taken to 0.'));
        
        body.add(startingSuppliesIntro);
        body.print();
    }
});