define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('damageAndHealing', 100, 100),
            damageAndHealingIntro = document.createElement("blockquote");

        var body = link(title, "Damage and Healing", wrapper);
        damageAndHealingIntro.appendChild(document.createTextNode('Keep track of Damage Taken as indiciated in the Basic System.  If your Damage Taken exceeds your Endurance (see your Character Record), you are unconscious.  If this occurs durin ga fight, you are defeated and must proceed as the text indicates.  If the text indiciates that you "wake up," reduce your Damage Taken to equal your Endurance.'));
        damageAndHealingIntro.appendChild(document.createElement('br'));
        damageAndHealingIntro.appendChild(document.createElement('br'));
        damageAndHealingIntro.appendChild(document.createTextNode('Do not use the Basic System rule for healing.  Each time you read a section of text that does not require you to a pick a number, fight, or take an action, you may reduce your Damage Taken by 1 for every 20 minutes you spend "resting".'));
        
        body.add(damageAndHealingIntro);
        body.print();
    }
});