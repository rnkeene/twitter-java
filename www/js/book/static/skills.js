define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('creatingCharacterSkills', 100, 100),
            createSkillsDetailsIntro = document.createElement("blockquote");
        var body = link(title, "Skills", wrapper);
        
        createSkillsDetailsIntro.appendChild(document.createTextNode('  The following 8 "skills" affect your chances of accomplishing certain actions during your adventures.'));
        createSkillsDetailsList = document.createElement("ol");
        var skillDetails = function(content){
            var item = document.createElement("li");
            item.appendChild(document.createTextNode(content));
            createSkillsDetailsList.appendChild(item);
        }
        skillDetails('Melee OB Skill: This skill reflects your ability to attack in melee (hand to hand) combat.  OB stands for "Offensive Bonus".')
        skillDetails('Missile OB Skill: This skill reflects your ability to attack using a missile such as a thrown spear or a bow.  OB stands for "Offensive Bonus" (not used in the Basic System)');
        skillDetails('General Skill: Use this skill when directed to perform general activities by the text, including: Climb, Track, Hunt, Ride, and Swim actions.');
        skillDetails('Trickery Skill: User this skill when trying to move without being seen or heard (i.e. sneaking), trying to steal or take something held or protected by an opponent, picking a lock, escaping from bonds, and other similar activities.');
        skillDetails('Perception Skill: This skill reflects how much information you gather through observation and exploration.  It also refelcts your ability to talk and negotiate with beings that you meet during your adventures.');
        skillDetails('Magical Skill: This skill reflects your affinity with magic and spells.  You use this skill when you try to cast a spell and when indicated by the text.');
        skillDetails('Running Skill: This skill reflects your chances of running away from danger.');
        skillDetails('DB Skill: This skill reflects your ability to avoid attacks.  DB stands for "Defensive Bonus".');
        
        var createSkillsBonusDetails = document.createElement("h3");
        createSkillsBonusDetails.appendChild(document.createTextNode('Skill Bonuses'));
        
        var createSkillsDetailsRecap = document.createElement("blockquote");
        createSkillsDetailsRecap.appendChild(document.createTextNode('For each of these skills, you will have a Skill Bonus used when you attempt certain actions.  Keep in mind that these "bonuses" can be negative as well as positive.'));
        
        createSkillsDetailsPoints = document.createElement("ul");
        var skillPoints = function(content){
            var item = document.createElement("li");
            item.appendChild(document.createTextNode(content));
            createSkillsDetailsPoints.appendChild(item);
        }
        skillPoints('When you start your character, you have 6 "+1 bonuses" to assign to your skills; the choice is yours (see below).  These bonuses may not be assigned to your "DB" skill or your "Running" skill.');
        skillPoints('You may assign more than one "+1 bonuses" to a given skill, but no more than three to any one skill.  Thus, two "+1 bonuses" assigned to a skill will be a "+2 bonus," and three "+1 bonuses" will be a "+3 bonus".  These bonuses should be recorded in the appropriate spaces in the Skill Bonus column on your Character Record.');
        skillPoints('If you do not assign any "+1 bonuses" to a skill, record a "-2 bonus" in the appropriate space.  The "DB" and "Running skills do not receive this "-2 bonus".');
        
        body.add(createSkillsDetailsIntro);
        body.add(createSkillsDetailsList);
        body.add(createSkillsBonusDetails);
        body.add(createSkillsDetailsRecap);
        body.add(createSkillsDetailsPoints);
        body.print();
    }
});