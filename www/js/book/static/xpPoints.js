define(["../static"], function(static){
    return function(title, wrapper){
        var link = static('experiencePoints', 100, 100),
            experiencePointsIntro = document.createElement("blockquote");

        var body = link(title, "Experience Points", wrapper);
        experiencePointsIntro.appendChild(document.createTextNode('After certain text passages, you will see a number of Experience Points.  Keep a running total of points in the space provided on your Character Record.  You may only receive experience points for a given text passage once.'));
        experiencePointsIntro.appendChild(document.createElement('br'));
        experiencePointsIntro.appendChild(document.createElement('br'));
        experiencePointsIntro.appendChild(document.createTextNode('These points have no affect on the abilities of your character until you have successfully completed this adventure and wish to start another Middle Earth Quest Gameobok with the same character.'));
        experiencePointsIntro.appendChild(document.createElement('br'));
        experiencePointsIntro.appendChild(document.createElement('br'));
        experiencePointsIntro.appendChild(document.createTextNode('If you are using a Middle Earth Quest Gamebook character, for every 150 experience points you may choose one of these options:.'));
        
        var experiencePointsListBlockquote = document.createElement('blockquote');
        var experiencePointsList = document.createElement("ol");
        var experiencePointItem = function(content){
            var item = document.createElement("li");
            item.appendChild(document.createTextNode(content));
            experiencePointsList.appendChild(item);
        }
        experiencePointItem('Assign an additional +1 bonus to any of the allowed skills; or');
        experiencePointItem('You may change any "-2" skill bonus to a "+1"; or');
        experiencePointItem('You may choose two more spells that you may cast; or');
        experiencePointItem('You may pick a number and increase your Endurance by 2 plus that number.');
        experiencePointsListBlockquote.appendChild(experiencePointsList)
        
        var merpDetails = document.createElement('blockquote');
        merpDetails.appendChild(document.createTextNode('If you are using Middle Earth Role Playing (MERP), 150 experience points is equivalent to approximately 5000 MERP points. '))
        
        body.add(experiencePointsIntro);
        body.add(experiencePointsListBlockquote);
        body.add(merpDetails);
        body.print();
    }
});