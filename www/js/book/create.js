define(["../div", '../style', '../hyperlink'], function(container, style, hyperlink) {
    var wrapper = container("bookDiv", 100, 100, 400, 600),
        bookDivStyle = style('bookDiv');
    bookDivStyle("background-color", "#ffffe6");
    bookDivStyle.promote();
    return function(ajax) {
        var secretKey, timeDiv, expDiv, pageLabelDiv, textarea, contentSubmit,
            nextPageDropdownDiv, nextPageKeyCodeDiv, nextPageLabelDiv, nextPageButtonDiv,
            breadcrumbs = [], breadcrumbDiv,
            cleanStory = function() {
                wrapper.div.removeChild(breadcrumbDiv);
                wrapper.div.removeChild(secretKey);
                wrapper.div.removeChild(timeDiv);
                wrapper.div.removeChild(expDiv);
                wrapper.div.removeChild(pageLabelDiv);
                wrapper.div.removeChild(textarea);
                wrapper.div.removeChild(contentSubmit);
            },
            nextPageCreateOn = function() {
                wrapper.div.appendChild(nextPageDropdownDiv);
                wrapper.div.appendChild(nextPageKeyCodeDiv);
                wrapper.div.appendChild(nextPageLabelDiv);
                wrapper.div.appendChild(nextPageButtonDiv);
            },
            nextPageCreateOff = function() {
                wrapper.div.removeChild(nextPageDropdownDiv);
                wrapper.div.removeChild(nextPageKeyCodeDiv);
                wrapper.div.removeChild(nextPageLabelDiv);
                wrapper.div.removeChild(nextPageButtonDiv);
            },
            refreshPageContent = function(response) {
                response.data = JSON.parse(response.response);
                var refreshContentCheck = function() {
                    if (response.data.pageLabel === 'prologue') {
                        populateContent();
                    } else {
                        populateNextContent(response.data.id);
                    }
                }
                breadcrumbs.push(JSON.parse(response.response));
                breadcrumbDiv = document.createElement("div");
                breadcrumbs.forEach(function(page, index, arr) {
                    var span = document.createElement("span");
                    var id = page.id, label=page.pageLabel, save=page;
                    span.addEventListener("mousedown", function(){                        
                        breadcrumbs.length = breadcrumbs.indexOf(save);
                        response.data = save;
                        cleanStory();
                        nextPageCreateOff();
                        refreshContentCheck();
                    }, true);
                    hyperlink(span, wrapper);
                    span.appendChild(document.createTextNode( ' > ' + label));
                    breadcrumbDiv.appendChild(span);
                });
                wrapper.div.appendChild(breadcrumbDiv);
                secretKey = document.createElement("input");
                secretKey.type = "text";
                wrapper.div.appendChild(secretKey);
                var exp = document.createElement("input"),
                 time = document.createElement("input");
                timeDiv = document.createElement("div"),
                    expDiv = document.createElement("div");
                exp.type = 'text';
                
                if(response.data.experiencePoints!==undefined){
                    exp.value = response.data.experiencePoints;
                }
                expDiv.appendChild(document.createTextNode("Experience Points: "));
                expDiv.appendChild(exp);
                timeDiv.appendChild(document.createTextNode("Time: "));
                time.type = 'text';
                if(response.data.time!==undefined){
                    time.value = response.data.time;
                }
                timeDiv.appendChild(time);                
                wrapper.div.appendChild(timeDiv);
                wrapper.div.appendChild(expDiv);
                pageLabelDiv = document.createElement("div");
                pageLabelDiv.appendChild(document.createTextNode("Current Page: " + response.data.pageLabel));
                wrapper.div.appendChild(pageLabelDiv);
                textarea = document.createElement("textarea");
                textarea.rows = "10";
                textarea.cols = "45";
                if (response.data.content) {
                    textarea.appendChild(document.createTextNode(response.data.content));
                }
                wrapper.div.appendChild(textarea);
                contentSubmit = document.createElement("button");
                contentSubmit.appendChild(document.createTextNode("Submit Page Content!"))
                contentSubmit.addEventListener("mousedown", function() {   
                    var service = ajax(function(data, status, headers, config) {
                        cleanStory();
                        nextPageCreateOff();
                        refreshContentCheck();
                        breadcrumbs.pop();
                    });
                    service.init('POST', '/rest/page/' + response.data.id + '/' + time.value + '/' + exp.value + '/'+ secretKey.value);
                    service.header('Content-Type', 'text/plain;charset=UTF-8');
                    service.send(textarea.value);
                }, true);
                wrapper.div.appendChild(contentSubmit);
                var nextPageDropdown = document.createElement("select"),
                    nextPageCreate = document.createElement("option"),
                    nextPageButton = document.createElement("button"),
                    nextPageLabel = document.createElement("input"),
                    nextPageKeyCode = document.createElement("input");

                nextPageDropdownDiv = document.createElement("div"),
                    nextPageKeyCodeDiv = document.createElement("div"),
                    nextPageLabelDiv = document.createElement("div"),
                    nextPageButtonDiv = document.createElement("div");
                nextPageCreate.value = "create";
                nextPageCreate.appendChild(document.createTextNode("Create New Page"));
                nextPageButton.appendChild(document.createTextNode("Create Page Label!"));
                nextPageDropdown.appendChild(nextPageCreate);
                nextPageDropdownDiv.appendChild(nextPageDropdown);
                nextPageKeyCodeDiv.appendChild(nextPageKeyCode);
                nextPageLabelDiv.appendChild(nextPageLabel);
                nextPageButtonDiv.appendChild(nextPageButton);
                var service = ajax(function(response) {
                    var data = JSON.parse(response.response);
                    data.forEach(function(page, index, arr) {
                        var item = document.createElement("option");
                        item.value = page.to.id;
                        item.appendChild(document.createTextNode(page.to.pageLabel))
                        nextPageDropdown.append(item);
                    });                    
                });
                service.init('GET', '/rest/next/' + response.data.id);
                service.send();                
                nextPageDropdown.addEventListener("change", function() {
                    if (dropdown.value === "create") {
                        nextPageCreateOn();
                    } else {
                        nextPageCreateOff();
                        cleanStory();
                        populateNextContent(nextPageDropdown.value);
                    }
                }, true);                
                nextPageButton.addEventListener("mousedown", function() {
                    var service = ajax(function(response) {
                        cleanStory();
                        nextPageCreateOff();
                        breadcrumbs.pop();
                        refreshContentCheck();
                    });
                    service.init('GET', '/rest/page/' + response.data.id + '/' + nextPageLabel.value + '/' + nextPageKeyCode.value);
                    serviec.send();
                }, true);
                nextPageLabel.type = "text";
                nextPageCreateOn();
            },
            populateNextContent = function(pageId) {
                 var service = ajax(function(response) {
                    refreshPageContent(response);
                });
                service.init('GET', '/rest/page/' + pageId);
                service.send();
            },
            populateContent = function() {
                var service = ajax(function(response) {
                    refreshPageContent(response);
                });
               service.init('GET', '/rest/page/start/' + dropdown.value);
                service.send();
            },
            changeDropdown = function() {
                if (dropdown.value === "create") {                    
                    breadcrumbs = [];
                    cleanStory();
                    nextPageCreateOff();
                    div.appendChild(createContainer);
                } else {
                    div.removeChild(createContainer);
                    populateContent();
                }
            },
            populateOptions = function() {
                var service = ajax(function(response) {
                    dropdown = document.createElement("select"),
                        create = document.createElement("option");
                    create.value = "create";
                    create.appendChild(document.createTextNode("Create New Book"));
                    dropdown.append(create);
                    dropdown.addEventListener("change", changeDropdown, true);
                    div.appendChild(dropdown);
                    div.appendChild(createContainer);
                    var list = JSON.parse(response.response),
                        item;
                    list.forEach(function(book, index, arr) {
                        item = document.createElement("option");
                        item.value = book.id;
                        item.appendChild(document.createTextNode(book.name))
                        dropdown.append(item);
                    })
                    wrapper.div.appendChild(div);
                });
               service.init('GET','/rest/books');
                service.send();
            }
        var div = document.createElement("div"),
            adminConsoleTitle = document.createElement('h2'),
            closeAdminConsole = document.createElement("div"),
            dropdown = document.createElement("select"),
            createContainer = document.createElement("div"),
            createLabel = document.createTextNode("Book Name: "),
            createDiv = document.createElement("div"),
            password = document.createElement("input"),
            passwordLabel = document.createTextNode("Secret Key: "),
            passwordDiv = document.createElement("div"),
            bookName = document.createElement("input"),
            submit = document.createElement("button"),
            dropdown, create,
            submitDiv = document.createElement("div");
        bookName.type = "text", password.type = "text";
        
        adminConsoleTitle.appendChild(document.createTextNode('Adventure Administration'));
        div.appendChild(adminConsoleTitle);
        closeAdminConsole.appendChild(document.createTextNode('Close Adventure Administration'));
        closeAdminConsole.appendChild(document.createElement('br'));
        closeAdminConsole.appendChild(document.createElement('br'));
        
        div.appendChild(closeAdminConsole);
        closeAdminConsole.addEventListener('mousedown', function(){
            parent.removeChild(wrapper.div);
        }, true);
        hyperlink(closeAdminConsole, wrapper);

        passwordDiv.appendChild(passwordLabel);
        passwordDiv.appendChild(password);
        createDiv.appendChild(createLabel);
        createDiv.appendChild(bookName);
        submit.appendChild(document.createTextNode("Create a New Book!"));
        submitDiv.appendChild(submit);
        createContainer.appendChild(passwordDiv);
        createContainer.appendChild(createDiv);
        createContainer.appendChild(submitDiv);


        submit.addEventListener("mousedown", function(event) {
            var service = ajax(function(response) {
                wrapper.div.removeChild(div);
                div.removeChild(dropdown);
                div.removeChild(createContainer);
                populateOptions();
            });
            service.init("POST", '/rest/book/' + password.value);
            service.header('Content-Type', 'application/json;charset=UTF-8');
            service.send(JSON.stringify({"name": bookName.value}));
        }, true);
        populateOptions();
        return wrapper;
    }
});