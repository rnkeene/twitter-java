define(["../div", "../style", '../ajax', "../hyperlink", '../misc/textInput'], function(container, style, ajax, hyperlink, smallInput) {
    var named = 'graphDiv';
    var wrapper = container(named, 50, 50, window.innerWidth-250, window.innerHeight-150), history=[[],[]];
    
    return function(){
        var dropdown, header, content,
            refreshPageContent = function(response) {
                content = document.createElement('div');
                var data = JSON.parse(response.response);
                var decorate = data.page.content.split("    "), run=true;
                while(decorate.length){
                    var current = decorate.shift();
                    if(decorate.length===0){
                        if(run){
                            run=false;
                            decorate = current.split('\n\n ');
                            current = decorate.shift();
                        }                        
                    }
                    var blockquote = document.createElement("blockquote");
                    blockquote.appendChild(document.createTextNode(current));
                    content.appendChild(blockquote);
                }
                wrapper.div.appendChild(content);
                var contentRule = style('content');
                contentRule("overflow-y", "scroll");
                contentRule.toggle(content);
                contentRule("height", wrapper.div.offsetHeight-350 + "px");
                contentRule.promote();
                document.addEventListener(named+'Resize', function(event){
                    contentRule("height", wrapper.div.offsetHeight-350 + "px");
                    contentRule.promote();
                }, true);
                var nextPage = document.createElement("div");
                nextPage.appendChild(document.createTextNode("Please select your next Page:"));
                content.appendChild(nextPage);
                var list = document.createElement("ul");
                var service = ajax(function(response){                    
                    var response = JSON.parse(response.response);
                    if(response.length){
                        response.forEach(function(page, index, arr) {
                            var item = document.createElement("li"), 
                                active = document.createElement('span');
                            active.appendChild(document.createTextNode(page.to.pageLabel));
                            active.addEventListener('mousedown', function(){
                                wrapper.div.removeChild(content);
                                populateNextContent(page.to.id);
                            }, true);
                            hyperlink(active, wrapper);
                            item.appendChild(active)
                            list.appendChild(item);
                        });
                    } else {
                        var checkoutService = ajax(function(response){
                            var checkout = JSON.parse(JSON.parse(response.response));
                            var id = checkout.data.checkoutCreate.checkout.id;
                            var url = checkout.data.checkoutCreate.checkout.webUrl.replace("keenedom.myshopify.com", "keenedom.com");
                            
                            var item = document.createElement('li'),
                                future = document.createElement('li'),
                                active = document.createElement('span'),
                                confirm = document.createElement('span');
                            
                            active.appendChild(document.createTextNode('Purchase Middle-Earth Quest Adventure Reset'));
                            confirm.appendChild(document.createTextNode('Press here when the purchase is complete.'));
                            hyperlink(active, wrapper);
                            hyperlink(confirm, wrapper);
                            item.appendChild(active);
                            future.appendChild(confirm);
                            list.appendChild(item);
                            list.appendChild(future);
                            
                            active.addEventListener('mousedown', function(){
                                window.open(url, "_blank");                                
                            }, true);
                            confirm.addEventListener('mousedown', function(){
                                wrapper.div.removeChild(content);
                                populateFutureContent(data.page.id, id);
                            }, true);
                        })
                        checkoutService.init("GET", '/rest/user/checkout');
                        checkoutService.send();
                    }
                    content.appendChild(list);
                });
                service.init("GET", '/rest/next/' + data.page.id);
                service.send();
            },
            populateFutureContent = function(pageId, checkoutId) {
                if(typeof user !== "undefined"){
                    var service = ajax(function(response) {
                        refreshPageContent(response);
                    });
                    service.init('GET', '/rest/user/reset/' + user.id + '/' + pageId + '/' + checkoutId);
                    service.send();
                }
            },
            populateNextContent = function(pageId) {
                if(typeof user !== "undefined"){
                    var service = ajax(function(response) {
                        refreshPageContent(response);
                    });
                    service.init('GET', '/rest/user/' + user.id + '/' + pageId);
                    service.send();
                }
            },
            populateContent = function() {
                if(typeof user !== "undefined"){
                    var service = ajax(function(response) {
                        refreshPageContent(response);
                    });
                    service.init('GET', '/rest/page/start/'+user.id+'/' + dropdown.value);
                    service.send();
                }
            },
            google, owner, readMenu,
            clearScreen = function(){
                wrapper.div.removeChild(content);
                dropdown.value='select';
                wrapper.div.removeChild(owner);
                wrapper.div.removeChild(readMenu);
                wrapper.div.removeChild(header);
                wrapper.div.appendChild(dropdown);
            },
            characterId,
            resetCharacter = function(){
                strengthValue.value = 0,
                strengthBonus.value = 0,
                agilityValue.value = 0,
                agilityBonus.value = 0,
                intelligenceValue.value = 0,
                intelligenceBonus.value = 0,
                enduranceValue.value = 0,
                enduranceStrength.value = 0,
                meleeOBTotal.value = 0,
                meleeOBSkillBonus.value = 0,
                meleeOBStatBonus.value = 0,
                meleeOBSpecialBonus.value = 0,
                missileOBTotal.value = 0,
                missileOBSkillBonus.value = 0,
                missileOBStatBonus.value = 0,
                missileOBSpecialBonus.value = 0,
                dbTotal.value = 0,
                dbSkillBonus.value = 0,
                dbStatBonus.value = 0,
                dbSpecialBonus.value = 0,
                runningTotal.value = 0,
                runningSkillBonus.value = 0,
                runningStatBonus.value = 0,
                runningSpecialBonus.value = 0,
                generalTotal.value = 0,
                generalSkillBonus.value = 0,
                generalStatBonus.value = 0,
                generalSpecialBonus.value = 0,
                trickeryTotal.value = 0,
                trickerySkillBonus.value = 0,
                trickeryStatBonus.value = 0,
                trickerySpecialBonus.value = 0,
                perceptionTotal.value = 0,
                perceptionSkillBonus.value = 0,
                perceptionStatBonus.value = 0,
                perceptionSpecialBonus.value = 0,
                magicalTotal.value = 0,
                magicalSkillBonus.value = 0,
                magicalStatBonus.value = 0,
                magicalSpecialBonus.value = 0,
                cloak.value = '',
                armor.value = '',
                dagger.value = '',
                belt.value = '',
                characterBackpackInput.value='',
                damageTaken.value = 0,
                minutes.value = 0,
                days.value = 0,
                experiencePoints.value = 0,
                characterName.value = '',                
                characterNotesTextarea.value = '';
                while(backpackItems.length){
                    characterBackpackDiv.removeChild(backpackItems.shift());
                }
            },
            showCreateRecord=false, character,createNone,createNew, createCharacterWrapper = document.createElement('span'),
            setCharacterRecordScrolling = function(){
                var height = characterRecord.div.offsetHeight;
                characterRecordDivStyle("overflow-y", "scroll");
                characterRecordDivStyle("height", height-150 + "px");                    
                characterRecordDivStyle.promote();
                document.addEventListener('characterRecordResize', function(event){
                    height = characterRecord.div.offsetHeight;
                    characterRecordDivStyle("height", height-150 + "px");
                    characterRecordDivStyle.promote();
                }, true);
            },
            loadCharacter = function(){
                if(character.value==='create'){
                    characterId=undefined;
                    resetCharacter();
                    showCreateRecord=true;
                    parent.appendChild(characterRecord.div);
                    characterRecord.bringForward();
                    setCharacterRecordScrolling();
                }else {
                    if(character.value==='none'){
                        resetCharacter();
                        if(showCreateRecord){
                            showCreateRecord=false;
                            parent.removeChild(characterRecord.div);
                        }
                    }else{
                        characterRecord.bringForward();
                        var index = characters[0].indexOf(character.value);
                        var toon = characters[1][index];                        
                        if(showCreateRecord===false){
                            showCreateRecord=true;
                            parent.appendChild(characterRecord.div);
                            setCharacterRecordScrolling();
                        }
                        characterId = toon.id,
                        strengthValue.value = toon.strengthValue,
                        strengthBonus.value = toon.strengthBonus,
                        agilityValue.value = toon.agilityValue,
                        agilityBonus.value = toon.agilityBonus,
                        intelligenceValue.value = toon.intelligenceValue,
                        intelligenceBonus.value = toon.intelligenceBonus,
                        enduranceValue.value = toon.enduranceValue,
                        enduranceStrength.value = toon.enduranceStrength,
                        meleeOBTotal.value = toon.meleeOBTotal,
                        meleeOBSkillBonus.value = toon.meleeOBSkillBonus,
                        meleeOBStatBonus.value = toon.meleeOBStatBonus,
                        meleeOBSpecialBonus.value = toon.meleeOBSpecialBonus,
                        missileOBTotal.value = toon.missileOBTotal,
                        missileOBSkillBonus.value = toon.missileOBSkillBonus,
                        missileOBStatBonus.value = toon.missileOBStatBonus,
                        missileOBSpecialBonus.value = toon.missileOBSpecialBonus,
                        dbTotal.value = toon.dbTotal,
                        dbSkillBonus.value = toon.dbSkillBonus,
                        dbStatBonus.value = toon.dbStatBonus,
                        dbSpecialBonus.value = toon.dbSpecialBonus,
                        runningTotal.value = toon.runningTotal,
                        runningSkillBonus.value = toon.runningSkillBonus,
                        runningStatBonus.value = toon.runningStatBonus,
                        runningSpecialBonus.value = toon.runningSpecialBonus,
                        generalTotal.value = toon.generalTotal,
                        generalSkillBonus.value = toon.generalSkillBonus,
                        generalStatBonus.value = toon.generalStatBonus,
                        generalSpecialBonus.value = toon.generalSpecialBonus,
                        trickeryTotal.value = toon.trickeryTotal,
                        trickerySkillBonus.value = toon.trickerySkillBonus,
                        trickeryStatBonus.value = toon.trickeryStatBonus,
                        trickerySpecialBonus.value = toon.trickerySpecialBonus,
                        perceptionTotal.value = toon.perceptionTotal,
                        perceptionSkillBonus.value = toon.perceptionSkillBonus,
                        perceptionStatBonus.value = toon.perceptionStatBonus,
                        perceptionSpecialBonus.value = toon.perceptionSpecialBonus,
                        magicalTotal.value = toon.magicalTotal,
                        magicalSkillBonus.value = toon.magicalSkillBonus,
                        magicalStatBonus.value = toon.magicalStatBonus,
                        magicalSpecialBonus.value = toon.magicalSpecialBonus,
                        cloak.value = toon.cloak,
                        armor.value = toon.armor,
                        dagger.value = toon.dagger,
                        belt.value = toon.belt,
                        damageTaken.value = toon.damageTaken,
                        minutes.value = toon.minutes,
                        days.value = toon.days,
                        experiencePoints.value = toon.experiencePoints,
                        characterName.value = toon.characterName,
                        characterNotesTextarea.value = toon.notes;
                        while(backpackItems.length){
                            characterBackpackDiv.removeChild(backpackItems.shift());
                        }
                        var service = ajax(function(response) {
                            var data = JSON.parse(response.response);
                            characterBackpackInput.value=data.shift().item;
                            data.forEach(function(item, index, arr) {
                                var nextBackpackItem = smallInput(30, item.item);
                                nextBackpackItem.addEventListener('change', function(){
                                    if(nextBackpackItem.value!==''){
                                        rebackpack();
                                    }else{
                                        var index = backpackItems.indexOf(nextBackpackItem);
                                        backpackItems.splice(index, 1);
                                        characterBackpackDiv.removeChild(nextBackpackItem);
                                    }
                                }, true);
                                backpackItems.push(nextBackpackItem);
                                characterBackpackDiv.appendChild(nextBackpackItem);
                            });
                            rebackpack();
                        });
                        service.init('GET', '/rest/backpack/'+characterId);
                        service.send();
                    }
                }
            },
            refreshCharacter = function(selected){
                if(createCharacterWrapper.contains(character)){
                    createCharacterWrapper.removeChild(character);
                }
                character = document.createElement("select");
                createNone = document.createElement("option"),
                    createNew = document.createElement("option");
                createNone.value='none';
                createNew.value='create';
                createNone.appendChild(document.createTextNode('Select Character Record'));
                createNew.appendChild(document.createTextNode('Create Character'));
                character.appendChild(createNone);
                character.appendChild(createNew);
                createCharacterWrapper.appendChild(character);
                var service = ajax(function(response) {
                    var data = JSON.parse(response.response);
                    data.forEach(function(toon, index, arr) {
                        var item = document.createElement("option");
                        item.value=toon.id;
                        item.appendChild(document.createTextNode(toon.characterName));
                        character.appendChild(item);
                        characters[0].push(toon.id+'');
                        characters[1].push(toon);
                        if(typeof selected!== undefined){
                            character.value = selected;
                        }
                    })
                    loadCharacter();
                });
                service.init('GET', '/rest/'+user.id+'/record');
                service.send();                
                character.addEventListener("change", function(){
                    if(characters[0].length){
                        characters=[[],[]];
                        refreshCharacter(character.value);
                    } else {
                        loadCharacter();
                    }
                }, true);  
            },
            adminConsole, socialDiv,
            changeDropdown = function() {                
                    if(google){
                        wrapper.div.removeChild(google);
                        google = undefined;
                    }
                    if(dropdown.value==='select'){
                        return;
                    }
                    wrapper.div.removeChild(dropdown);
                    var index = history[0].indexOf(dropdown.value);
                    header = document.createElement("h1");
                    header.appendChild(document.createTextNode(history[1][index]));
                    wrapper.div.appendChild(header);
                    owner = document.createElement("div");
                    owner.appendChild(document.createTextNode(user.fullName));
                    wrapper.div.appendChild(owner);
                    readMenu = document.createElement("div");
                    var characterLore = document.createElement("span");
                    characterLore.appendChild(document.createTextNode("Character Lore"));
                    hyperlink(characterLore, wrapper);
                    characterLore.addEventListener("mousedown", function(){                        
                        require(["./merp/toons"], function(body){
                            body(wrapper, parent);
                        });
                    }, true);
                    readMenu.appendChild(document.createElement('br'));
                    readMenu.appendChild(characterLore);
                    readMenu.appendChild(document.createElement('br'));
                    
                    var changeAdventure = document.createElement("span")
                    changeAdventure.appendChild(document.createElement("br"));
                    changeAdventure.appendChild(document.createTextNode(' Change Adventure '));                    
                    hyperlink(changeAdventure, wrapper);
                    changeAdventure.addEventListener('mousedown', function(){
                        clearScreen();
                    }, true);
                    readMenu.appendChild(changeAdventure);
                    
                    refreshCharacter('none');
                    var pickNumber = document.createElement("span"),
                        numberResult = document.createElement("span");
                    readMenu.appendChild(createCharacterWrapper);
                    pickNumber.appendChild(document.createTextNode(' Pick a Number: '));
                    pickNumber.appendChild(numberResult);
                    readMenu.appendChild(pickNumber);
                    
                    pickNumber.addEventListener('mousedown', function(){
                        pickNumber.removeChild(numberResult);
                        numberResult = document.createElement("span"),
                        numberResult.appendChild(document.createTextNode( Math.ceil(Math.random() * 6) + ', ' + Math.ceil(Math.random() * 6)));
                        pickNumber.appendChild(numberResult);
                    }, true);
                    hyperlink(pickNumber, wrapper);
                    
                    readMenu.appendChild(document.createElement("br"));
                    readMenu.appendChild(document.createElement("br"));
                    
                    var takeActionTitle = document.createElement('span'),
                        fightingTitle = document.createElement('span'),
                        adminConsoleTitle = document.createElement('span'),
                        socialMediaTitle = document.createElement('span');
                    takeActionTitle.appendChild(document.createTextNode('Taking an Action. ')),
                        fightingTitle.appendChild(document.createTextNode(' Fighting.')),
                        adminConsoleTitle.appendChild(document.createTextNode(' Adventure Administration.')),
                        socialMediaTitle.appendChild(document.createTextNode(' Social Media Post.')),
                        require(["./book/static/fighting"], function(body){
                            body(fightingTitle, wrapper);
                        }),
                        require(["./book/static/action"], function(body){
                            body(takeActionTitle, wrapper);
                        });
                    
                    hyperlink(adminConsoleTitle, wrapper);
                    hyperlink(socialMediaTitle, wrapper);
                    
                    adminConsoleTitle.addEventListener('mousedown', function(){
                        adminConsole.bringForward();
                        parent.appendChild(adminConsole.div);
                    }, true);
                    
                    socialMediaTitle.addEventListener('mousedown', function(){
                        socialDiv.bringForward();
                        parent.appendChild(socialDiv.div);
                    }, true);
                    
                    readMenu.appendChild(takeActionTitle);
                    readMenu.appendChild(fightingTitle);
                    readMenu.appendChild(adminConsoleTitle);
                    readMenu.appendChild(socialMediaTitle);
                    
                    readMenu.appendChild(document.createElement("br"));
                    readMenu.appendChild(document.createElement("br"));
                    
                    wrapper.div.appendChild(readMenu);
                    populateContent();
                    document.addEventListener("userLogin", clearScreen, true);                
            };
        var characters = [[],[]];
        
        var characterRecord = container('characterRecord', 60, 60, 500, window.innerHeight-200),
            characterRecordDiv = document.createElement("div");
        characterRecordDiv.addEventListener('mouseover', function(){
            characterRecord.toggle(true);
        }, true);
        characterRecordDiv.addEventListener('mouseout', function(){
            characterRecord.toggle(false);
        }, true);
        var characterRecordStyle = style('characterRecord');
        var characterRecordDivStyle = style('characterRecordDiv');
        characterRecordDivStyle.toggle(characterRecordDiv);
        var characterRecordTitle = document.createElement("h2");
        var closeCharacterRecord = document.createElement("span");
        closeCharacterRecord.appendChild(document.createTextNode('Close Character Record'));
        closeCharacterRecord.appendChild(document.createElement("br"));
        closeCharacterRecord.appendChild(document.createElement("br"));
        closeCharacterRecord.addEventListener('mousedown', function(){
            character.value='none';
            showCreateRecord=false;
            parent.removeChild(characterRecord.div);            
        }, true);
        hyperlink(closeCharacterRecord, characterRecord);
        
        characterRecordTitle.appendChild(document.createTextNode('Character Record'));        
        var characterRecordStyle = style('characterRecord');
        characterRecordStyle("background-color", "#ffffe6");
        characterRecordStyle.promote();
        var characterNameDiv = document.createElement("div");
        characterNameDiv.appendChild(document.createTextNode('Name: ')),
            characterName =smallInput(30, ''),
            characterStatsTitle = document.createElement("h3"),
            characterStrengthDiv = document.createElement("div"),
            characterAgilityDiv = document.createElement("div"),
            characterIntelligenceDiv = document.createElement("div"),
            characterEnduranceDiv = document.createElement("div"),
            characterMeleeOBDiv = document.createElement("div"),
            characterSkillsTitle = document.createElement("h3"),
            characterMissileOBDiv = document.createElement("div"),
            characterDBDiv = document.createElement("div"),
            characterRunningDiv = document.createElement("div"),
            characterGeneralDiv = document.createElement("div"),
            characterTrickeryDiv = document.createElement("div"),
            characterPerceptionDiv = document.createElement("div"),
            characterMagicalDiv = document.createElement("div"),
            characterEquipmentTitle = document.createElement("h3"),
            characterCloakDiv = document.createElement("div"),
            characterArmorDiv = document.createElement("div"),
            characterDaggerDiv = document.createElement("div"),
            characterBeltDiv = document.createElement("div"),
            characterBackpackDiv = document.createElement("div"),
            characterDamageTakenDiv = document.createElement("div"),
            characterTimeMinutesDiv = document.createElement("div"),
            characterTimeDaysDiv = document.createElement("div"),
            characterExperiencePointsDiv = document.createElement("div"),
            characterNotesDiv = document.createElement("div"),
            characterSubmitDiv = document.createElement("div");
        
        characterName.type = 'text';
        characterNameDiv.appendChild(characterName),
            characterStatsTitle.appendChild(document.createTextNode('Stats [value : bonus]')),
            characterStrengthDiv.appendChild(document.createTextNode('Strength (St): ')),
            characterAgilityDiv.appendChild(document.createTextNode('Agility (Ag): ')),
            characterIntelligenceDiv.appendChild(document.createTextNode('Intelligence (In): ')),
            characterEnduranceDiv.appendChild(document.createTextNode('Endurance: ')),
            characterSkillsTitle.appendChild(document.createTextNode('Skills [Total : Skill Bonus : Stat Bonus : Special Bonus]')),
            characterMeleeOBDiv.appendChild(document.createTextNode('Melee OB: ')),
            characterMissileOBDiv.appendChild(document.createTextNode('Missile OB: ')),
            characterDBDiv.appendChild(document.createTextNode('DB: ')),
            characterRunningDiv.appendChild(document.createTextNode('Running: ')),
            characterGeneralDiv.appendChild(document.createTextNode('General: ')),
            characterTrickeryDiv.appendChild(document.createTextNode('Trickery: ')),
            characterPerceptionDiv.appendChild(document.createTextNode('Perception: ')),
            characterMagicalDiv.appendChild(document.createTextNode('Magical: ')),
            characterEquipmentTitle.appendChild(document.createTextNode('Equipment'));
             var startingSupplies = document.createElement('span');
            startingSupplies.appendChild(document.createTextNode('Starting Supplies'));
            startingSupplies.appendChild(document.createElement('br'));
            startingSupplies.appendChild(document.createElement('br'));
            characterCloakDiv.appendChild(document.createTextNode('Cloak: ')),
            characterArmorDiv.appendChild(document.createTextNode('Armor: ')),
            characterDaggerDiv.appendChild(document.createTextNode('Dagger: ')),
            characterBeltDiv.appendChild(document.createTextNode('Belt: ')),
            characterBackpackDiv.appendChild(document.createTextNode('Backpack: '));
            var damageAndHealing = document.createElement('span');
            damageAndHealing.appendChild(document.createTextNode('Damage Taken: '));
            characterDamageTakenDiv.appendChild(damageAndHealing),
            characterTimeMinutesDiv.appendChild(document.createTextNode('Minutes: ')),
            characterTimeDaysDiv.appendChild(document.createTextNode('Days: '));
            var experiencePointsTitle = document.createElement('span');
            experiencePointsTitle.appendChild(document.createTextNode('Experience Points: '));
            characterExperiencePointsDiv.appendChild(experiencePointsTitle),
            characterNotesDiv.appendChild(document.createTextNode('Notes: '));
        characterNotesDiv.appendChild(document.createElement('br'));
                
        require(["./book/static/stats"], function(body){
            body(characterStatsTitle, wrapper);
        });
                
        require(["./book/static/skills"], function(body){
            body(characterSkillsTitle, wrapper);
        });
        
        require(["./book/static/equipment"], function(body){
            body(characterEquipmentTitle, wrapper);
        });
        
        require(["./book/static/damage"], function(body){
            body(damageAndHealing, wrapper);
        });
        
        require(["./book/static/xpPoints"], function(body){
            body(experiencePointsTitle, wrapper);
        });
        
        require(["./book/static/supplies"], function(body){
            body(startingSupplies, wrapper);
        });
        
        parent = document.getElementById('SocialMessage');
        require(["./content/socialPost"], function(socialContent) {
            socialDiv = container("socialDiv", 100, 100, 400, 450);
            var loginDiv = container("loginDivWrapper", window.innerWidth-140, 20, 120, 50),
                loginWrapper = document.getElementById('contentWrapper');

            var socialDivTitle = document.createElement('h2'),
                closeSocialDiv = document.createElement('div');
            socialDivTitle.appendChild(document.createTextNode('Social Media Post'));
            socialDiv.div.appendChild(socialDivTitle);
            closeSocialDiv.appendChild(document.createTextNode("Close Social Media Post"));
            closeSocialDiv.appendChild(document.createElement('br'));
            closeSocialDiv.appendChild(document.createElement('br'));
            closeSocialDiv.addEventListener('mousedown', function(){                
                parent.removeChild(socialDiv.div);
            }, true);
            
            
            
            hyperlink(closeSocialDiv, socialDiv);
            socialDiv.div.appendChild(closeSocialDiv);
            socialDivStyle = style('socialDiv');
            socialDivStyle("background-color", "#ffffe6");
            socialDivStyle.promote();
            loginDiv.div.appendChild(loginWrapper);
            parent.appendChild(loginDiv.div);
            var content = socialContent();
            var toggleOn = function() {
                    content.removeEventListener("mouseover", toggleOn, true);
                    content.addEventListener("mouseout", toggleOff, true);
                    socialDiv.toggle(true);
                },
                toggleOff = function() {
                    socialDiv.toggle(false);
                    content.removeEventListener("mouseout", toggleOff, true);
                    content.addEventListener("mouseover", toggleOn, true);
                }
            content.addEventListener("mouseover", toggleOn, true)
            socialDiv.div.appendChild(content);
        });
        
        require(["./book/create"], function(create){
            adminConsole = create(ajax);
        });
        
        var strengthValue = smallInput(4, 0),
            strengthBonus = smallInput(4, 0),
            agilityValue = smallInput(4, 0),
            agilityBonus = smallInput(4, 0),
            intelligenceValue = smallInput(4, 0),
            intelligenceBonus = smallInput(4, 0),
            enduranceValue = smallInput(4, 0),
            enduranceStrength = smallInput(4, 0),
            meleeOBTotal = smallInput(4, 0),
            meleeOBSkillBonus = smallInput(4, 0),
            meleeOBStatBonus = smallInput(4, 0),
            meleeOBSpecialBonus = smallInput(4, 0),
            missileOBTotal = smallInput(4, 0),
            missileOBSkillBonus = smallInput(4, 0),
            missileOBStatBonus = smallInput(4, 0),
            missileOBSpecialBonus = smallInput(4, 0),
            dbTotal = smallInput(4, 0),
            dbSkillBonus = smallInput(4, 0),
            dbStatBonus = smallInput(4, 0),
            dbSpecialBonus = smallInput(4, 0),
            runningTotal = smallInput(4, 0),
            runningSkillBonus = smallInput(4, 0),
            runningStatBonus = smallInput(4, 0),
            runningSpecialBonus = smallInput(4, 0),
            generalTotal = smallInput(4, 0),
            generalSkillBonus = smallInput(4, 0),
            generalStatBonus = smallInput(4, 0),
            generalSpecialBonus = smallInput(4, 0),
            trickeryTotal = smallInput(4, 0),
            trickerySkillBonus = smallInput(4, 0),
            trickeryStatBonus = smallInput(4, 0),
            trickerySpecialBonus = smallInput(4, 0),
            perceptionTotal = smallInput(4, 0),
            perceptionSkillBonus = smallInput(4, 0),
            perceptionStatBonus = smallInput(4, 0),
            perceptionSpecialBonus = smallInput(4, 0),
            magicalTotal = smallInput(4, 0),
            magicalSkillBonus = smallInput(4, 0),
            magicalStatBonus = smallInput(4, 0),
            magicalSpecialBonus = smallInput(4, 0),
            cloak = smallInput(30, ''),
            armor = smallInput(30, ''),
            dagger = smallInput(30, ''),
            belt = smallInput(30, ''),
            damageTaken = smallInput(6, 0),
            minutes = smallInput(6, 0),
            days = smallInput(6, 0),
            experiencePoints = smallInput(6, 0),
            characterNotesTextarea = document.createElement('textarea');        
        
        characterNotesTextarea.rows='10';
        characterNotesTextarea.cols='50';
        
        characterStrengthDiv.appendChild(strengthValue),
            characterAgilityDiv.appendChild(agilityValue),
            characterIntelligenceDiv.appendChild(intelligenceValue),
            characterEnduranceDiv.appendChild(enduranceValue),
            characterMeleeOBDiv.appendChild(meleeOBTotal),
            characterMissileOBDiv.appendChild(missileOBTotal),
            characterDBDiv.appendChild(dbTotal),
            characterRunningDiv.appendChild(runningTotal),
            characterGeneralDiv.appendChild(generalTotal),
            characterTrickeryDiv.appendChild(trickeryTotal),
            characterPerceptionDiv.appendChild(perceptionTotal),
            characterMagicalDiv.appendChild(magicalTotal),
            characterCloakDiv.appendChild(cloak),
            characterArmorDiv.appendChild(armor),
            characterDaggerDiv.appendChild(dagger),
            characterBeltDiv.appendChild(belt),            
            characterDamageTakenDiv.appendChild(damageTaken),
            characterTimeMinutesDiv.appendChild(minutes),
            characterTimeDaysDiv.appendChild(days),
            characterExperiencePointsDiv.appendChild(experiencePoints),
            characterNotesDiv.appendChild(characterNotesTextarea);
        
            
        characterEnduranceDiv.appendChild(document.createTextNode(' = 20 + (2 x St Stat) ')),
            characterMeleeOBDiv.appendChild(document.createTextNode(' = ')),
            characterMissileOBDiv.appendChild(document.createTextNode(' = ')),
            characterDBDiv.appendChild(document.createTextNode(' = ')),
            characterRunningDiv.appendChild(document.createTextNode(' = ')),
            characterGeneralDiv.appendChild(document.createTextNode(' = ')),
            characterTrickeryDiv.appendChild(document.createTextNode(' = ')),
            characterPerceptionDiv.appendChild(document.createTextNode(' = ')),
            characterMagicalDiv.appendChild(document.createTextNode(' = '));
        
        characterStrengthDiv.appendChild(strengthBonus),
            characterAgilityDiv.appendChild(agilityBonus),
            characterIntelligenceDiv.appendChild(intelligenceBonus),
            characterEnduranceDiv.appendChild(enduranceStrength),
            characterMeleeOBDiv.appendChild(meleeOBSkillBonus),
            characterMissileOBDiv.appendChild(missileOBSkillBonus),
            characterDBDiv.appendChild(dbSkillBonus),
            characterRunningDiv.appendChild(runningSkillBonus),
            characterGeneralDiv.appendChild(generalSkillBonus),
            characterTrickeryDiv.appendChild(trickerySkillBonus),
            characterPerceptionDiv.appendChild(perceptionSkillBonus),
            characterMagicalDiv.appendChild(magicalSkillBonus);
            
        characterMeleeOBDiv.appendChild(document.createTextNode(' + ')),
            characterMissileOBDiv.appendChild(document.createTextNode(' + ')),
            characterDBDiv.appendChild(document.createTextNode(' + ')),
            characterRunningDiv.appendChild(document.createTextNode(' + ')),
            characterGeneralDiv.appendChild(document.createTextNode(' + ')),
            characterTrickeryDiv.appendChild(document.createTextNode(' + ')),
            characterPerceptionDiv.appendChild(document.createTextNode(' + ')),
            characterMagicalDiv.appendChild(document.createTextNode(' + '));
        
        characterMeleeOBDiv.appendChild(meleeOBStatBonus),
            characterMissileOBDiv.appendChild(missileOBStatBonus),
            characterDBDiv.appendChild(dbStatBonus),
            characterRunningDiv.appendChild(runningStatBonus),
            characterGeneralDiv.appendChild(generalStatBonus),
            characterTrickeryDiv.appendChild(trickeryStatBonus),
            characterPerceptionDiv.appendChild(perceptionStatBonus),
            characterMagicalDiv.appendChild(magicalStatBonus);
        
        characterMeleeOBDiv.appendChild(document.createTextNode(' St + ')),
            characterMissileOBDiv.appendChild(document.createTextNode(' Ag + ')),
            characterDBDiv.appendChild(document.createTextNode(' Ag + ')),
            characterRunningDiv.appendChild(document.createTextNode(' Ag + ')),
            characterGeneralDiv.appendChild(document.createTextNode(' Ag + ')),
            characterTrickeryDiv.appendChild(document.createTextNode(' In + ')),
            characterPerceptionDiv.appendChild(document.createTextNode(' In + ')),
            characterMagicalDiv.appendChild(document.createTextNode(' In + '));
        
        characterMeleeOBDiv.appendChild(meleeOBSpecialBonus),
            characterMissileOBDiv.appendChild(missileOBSpecialBonus),
            characterDBDiv.appendChild(dbSpecialBonus),
            characterRunningDiv.appendChild(runningSpecialBonus),
            characterGeneralDiv.appendChild(generalSpecialBonus),
            characterTrickeryDiv.appendChild(trickerySpecialBonus),
            characterPerceptionDiv.appendChild(perceptionSpecialBonus),
            characterMagicalDiv.appendChild(magicalSpecialBonus);
        
        
        var characterBackpackInput = smallInput(30, ''),
            backpackItems=[], 
            additem = function(){
                var nextBackpackItem = smallInput(30, '');
                    nextBackpackItem.addEventListener('change', function(){
                        if(nextBackpackItem.value!==''){
                            rebackpack();
                        }else{
                            var index = backpackItems.indexOf(nextBackpackItem);
                            backpackItems.splice(index, 1);
                            characterBackpackDiv.removeChild(nextBackpackItem);
                        }
                    }, true);
                    backpackItems.push(nextBackpackItem);
                    characterBackpackDiv.appendChild(nextBackpackItem);    
            },
            rebackpack = function(){
                if(!(backpackItems[backpackItems.length-1])){
                    additem();
                }else if(backpackItems[backpackItems.length-1].value!==''){
                    additem();
                }                
            }
        characterBackpackInput.addEventListener('change', function(){
            if(characterBackpackInput.value!==''){
                rebackpack();
            }
        }, true);
        characterBackpackDiv.appendChild(characterBackpackInput);
        
        var characterSubmit = document.createElement("button");
        characterSubmit.appendChild(document.createTextNode(" Submit Character Record"));
        characterSubmit.addEventListener('mousedown', function(){
            var record = {
                    "strengthValue": strengthValue.value,
                    "strengthBonus" : strengthBonus.value,
                    "agilityValue" : agilityValue.value,
                    "agilityBonus" : agilityBonus.value,
                    "intelligenceValue" : intelligenceValue.value,
                    "intelligenceBonus" : intelligenceBonus.value,
                    "enduranceValue" : enduranceValue.value,
                    "enduranceStrength" : enduranceStrength.value,
                    "meleeOBTotal" : meleeOBTotal.value,
                    "meleeOBSkillBonus" : meleeOBSkillBonus.value,
                    "meleeOBStatBonus" : meleeOBStatBonus.value,
                    "meleeOBSpecialBonus" : meleeOBSpecialBonus.value,
                    "missileOBTotal" : missileOBTotal.value,
                    "missileOBSkillBonus" : missileOBSkillBonus.value,
                    "missileOBStatBonus" : missileOBStatBonus.value,
                    "missileOBSpecialBonus" : missileOBSpecialBonus.value,
                    "dbTotal" : dbTotal.value,
                    "dbSkillBonus" : dbSkillBonus.value,
                    "dbStatBonus" : dbStatBonus.value,
                    "dbSpecialBonus" : dbSpecialBonus.value,
                    "runningTotal" : runningTotal.value,
                    "runningSkillBonus" : runningSkillBonus.value,
                    "runningStatBonus" : runningStatBonus.value,
                    "runningSpecialBonus" : runningSpecialBonus.value,
                    "generalTotal" : generalTotal.value,
                    "generalSkillBonus" : generalSkillBonus.value,
                    "generalStatBonus" : generalStatBonus.value,
                    "generalSpecialBonus" : generalSpecialBonus.value,
                    "trickeryTotal" : trickeryTotal.value,
                    "trickerySkillBonus" : trickerySkillBonus.value,
                    "trickeryStatBonus" : trickeryStatBonus.value,
                    "trickerySpecialBonus" : trickerySpecialBonus.value,
                    "perceptionTotal" : perceptionTotal.value,
                    "perceptionSkillBonus" : perceptionSkillBonus.value,
                    "perceptionStatBonus" : perceptionStatBonus.value,
                    "perceptionSpecialBonus" : perceptionSpecialBonus.value,
                    "magicalTotal" : magicalTotal.value,
                    "magicalSkillBonus" : magicalSkillBonus.value,
                    "magicalStatBonus" : magicalStatBonus.value,
                    "magicalSpecialBonus" : magicalSpecialBonus.value,
                    "cloak" : cloak.value,
                    "armor" : armor.value,
                    "dagger" : dagger.value,
                    "belt" : belt.value,
                    "damageTaken" : damageTaken.value,
                    "minutes" : minutes.value,
                    "days" : days.value,
                    "experiencePoints" : experiencePoints.value,
                    "characterName" : characterName.value,
                    "notes" : characterNotesTextarea.value
                };
            if(typeof characterId !== undefined){
                record["id"] = characterId;
            }
            var items =[];
            if(characterBackpackInput.value!==''){
                items.push(characterBackpackInput.value);
            }
            backpackItems.forEach(function(item, index, arr) {
                if(item.value!==''){
                    items.push(item.value);
                }
            });
            var service = ajax(function(response) { 
                var recordId = response.response;
                var subservice = ajax(function() {
                    showCreateRecord=false;
                    resetCharacter();
                    characters=[[],[]];
                    refreshCharacter(recordId);
                });
                subservice.init('POST', '/rest/backpack/'+recordId);
                subservice.header('Content-Type', 'application/json;charset=UTF-8');
                subservice.send(JSON.stringify(items));           
            });
            service.init('POST', '/rest/'+user.id+'/record');
            service.header('Content-Type', 'application/json;charset=UTF-8');
            service.send(JSON.stringify(record));            
        }, true);
        characterSubmitDiv.appendChild(characterSubmit);
        
        
        characterRecordDiv.appendChild(characterRecordTitle);
        characterRecordDiv.appendChild(closeCharacterRecord);
        characterRecordDiv.appendChild(characterNameDiv);
        characterRecordDiv.appendChild(characterStatsTitle);
        characterRecordDiv.appendChild(characterStrengthDiv);
        characterRecordDiv.appendChild(characterAgilityDiv);
        characterRecordDiv.appendChild(characterIntelligenceDiv);
        characterRecordDiv.appendChild(characterEnduranceDiv);
        characterRecordDiv.appendChild(characterSkillsTitle);
        characterRecordDiv.appendChild(characterMeleeOBDiv);
        characterRecordDiv.appendChild(characterMissileOBDiv);
        characterRecordDiv.appendChild(characterDBDiv);
        characterRecordDiv.appendChild(characterRunningDiv);
        characterRecordDiv.appendChild(characterGeneralDiv);
        characterRecordDiv.appendChild(characterTrickeryDiv);
        characterRecordDiv.appendChild(characterPerceptionDiv);
        characterRecordDiv.appendChild(characterMagicalDiv);
        characterRecordDiv.appendChild(characterEquipmentTitle);
        characterRecordDiv.appendChild(startingSupplies);        
        characterRecordDiv.appendChild(characterCloakDiv);
        characterRecordDiv.appendChild(characterArmorDiv);
        characterRecordDiv.appendChild(characterDaggerDiv);
        characterRecordDiv.appendChild(characterBeltDiv);
        characterRecordDiv.appendChild(characterBackpackDiv);
        characterRecordDiv.appendChild(characterDamageTakenDiv);
        characterRecordDiv.appendChild(characterTimeMinutesDiv);
        characterRecordDiv.appendChild(characterTimeDaysDiv);
        characterRecordDiv.appendChild(characterExperiencePointsDiv);
        characterRecordDiv.appendChild(characterNotesDiv);
        characterRecordDiv.appendChild(characterSubmitDiv);
        characterRecord.div.appendChild(characterRecordDiv);
        
        window.addEventListener('resize', function(){
            var wrapperStyle = style(named);
            wrapperStyle('width', window.innerWidth-250+"px");
            wrapperStyle('height', window.innerHeight-150+"px");
            wrapperStyle.promote();
            
            characterRecordStyle("height", (window.innerHeight-200) + "px");            
            characterRecordStyle.promote();
            height = characterRecord.div.offsetHeight;
            characterRecordDivStyle("height", height-150 + "px");
            characterRecordDivStyle.promote();
            
            var contentRule = style('content');
            contentRule("height", wrapper.div.offsetHeight-350 + "px");
            contentRule.promote();
        }, true);
        var loadedEvent = function(){
            var service = ajax(function successCallback(response) {
                dropdown = document.createElement("select");
                dropdown.addEventListener("change", changeDropdown, true);                
                var list = JSON.parse(response.response),
                    item = document.createElement("option");
                item.value='select';
                item.appendChild(document.createTextNode('Select your Adventure'));
                dropdown.append(item);
                list.forEach(function(book, index, arr) {
                    item = document.createElement("option");
                    item.value = book.id;
                    item.appendChild(document.createTextNode(book.name))
                    dropdown.append(item);
                    history[0].push(book.id+''),
                        history[1].push(book.name);
                })
                wrapper.div.appendChild(dropdown);
                changeDropdown();
            });
            service.init("GET", '/rest/books');
            service.send();
        }
        if(typeof user === "undefined"){
            if(typeof google === "undefined"){
                google = document.createElement("div");
                var createUser = document.createElement("span");
                createUser.appendChild(document.createTextNode('create an account here'));
                createUser.addEventListener('mousedown', function(){
                    window.open('https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp', '_blank');
                }, true);
                hyperlink(createUser, wrapper);
                google.appendChild(document.createTextNode('Please login with a Google account; or '));
                google.appendChild(createUser);
                google.appendChild(document.createTextNode(',  and then login.'));
                wrapper.div.appendChild(google);                
            }
            document.addEventListener('userLogin', loadedEvent, true);
        }else{
            loadedEvent();
        }  
        return wrapper;
    }
});