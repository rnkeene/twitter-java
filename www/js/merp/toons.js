define(["../div", "../style", "../hyperlink", '../misc/textInput', '../misc/multiTextInput', '../ajax'], function(container, style, hyperlink, smallInput, multiInput, ajax) {
    return function(wrapper, parent) {
        var named = 'MERPCharacters';
        var root = container(named, 100, 100, 500, window.innerHeight - 200),
            rootStyle = style(named);
        rootStyle("background-color", "#ffffe6");
        rootStyle.promote();

        window.addEventListener('resize', function() {
            rootStyle("height", (window.innerHeight - 200) + 'px');
            rootStyle.promote();            
        });
        parent.appendChild(root.div);

        root.div.addEventListener("mouseover", function() {
            wrapper.toggle(true);
        }, true);
        root.div.addEventListener("mouseout", function() {
            wrapper.toggle(false);
        }, true);
        var characterTitle = document.createElement('h2'),
            closeCharacterDiv = document.createElement('div');

        characterTitle.appendChild(document.createTextNode('Character Lore'));
        root.div.appendChild(characterTitle);
        closeCharacterDiv.appendChild(document.createTextNode("Close Character Lore"));
        closeCharacterDiv.appendChild(document.createElement('br'));
        closeCharacterDiv.appendChild(document.createElement('br'));
        closeCharacterDiv.addEventListener('mousedown', function() {
            parent.removeChild(root.div);
        }, true);

        var newCharacterDiv = document.createElement('div');
        newCharacterDiv.appendChild(document.createTextNode("New Character Lore"));
        newCharacterDiv.appendChild(document.createElement('br'));
        newCharacterDiv.appendChild(document.createElement('br'));
        hyperlink(closeCharacterDiv, root);
        hyperlink(newCharacterDiv, root);

        root.div.appendChild(closeCharacterDiv);
        root.div.appendChild(newCharacterDiv);
        var tableBuilder = function(columns){
            var table = document.createElement('table'),
                row = document.createElement('tr');
            table.appendChild(row);
            var count = 0;            
            return function(element){
                var cell = document.createElement('td');
                cell.appendChild(element);
                row.appendChild(cell);
                if((++count%columns)===0){
                    row = document.createElement('tr');
                    table.appendChild(row);
                }
                return table;
            }
        }
        var characterLore = document.createElement('div'), 
            characters = [],populate=function(){
                root.div.removeChild(characterLore);
                characterLore = document.createElement('div');                
                    var print = tableBuilder(5),
                        table;
                characters.forEach(function(toon){
                    var data = document.createElement('span');
                    data.appendChild(document.createTextNode(toon.name));
                    hyperlink(data, root);
                    data.addEventListener('mousedown', function(){
                        populateManageCharacter(toon);
                    }, true)
                    table = print(data);
                });
                if(table){
                    characterLore.appendChild(table);
                }
                
                loreStyle = style('characterLore');
                loreStyle("overflow-y", "scroll");
                loreStyle("height", root.div.offsetHeight-175 + "px");
                loreStyle.promote();
                loreStyle.toggle(characterLore);
                window.addEventListener('resize', function() {
                    height = root.div.offsetHeight;
                    loreStyle("height", root.div.offsetHeight-175 + 'px');
                    loreStyle.promote();
                });
                document.addEventListener(named + 'Resize', function(event){                    
                    loreStyle("height", root.div.offsetHeight-175 + "px");
                    loreStyle.promote();
                }, true);
                characterLore.addEventListener('mouseover', function(){
                    root.toggle(true);
                }, true);
                characterLore.addEventListener('mouseout', function(){
                    root.toggle(false);
                }, true);
                root.div.appendChild(characterLore);
                
            }, refresh = function(){
            var service = ajax(function(response) {
                characters = JSON.parse(response.response);
                populate();
            });
            service.init('GET', '/rest/merp');
            service.send();
        }
        refresh();
        root.div.appendChild(characterLore);
        
        var populateManageCharacter = function(character) {
            
            var static = container(named + 'Modal', 150, 150, 500, window.innerHeight - 200),
                editCharacterTitle = document.createElement('h2'),
                modalDiv = document.createElement('div'),
                secretKey = smallInput(30, ''),
                charName = smallInput(30, ''),
                notes = document.createElement('textarea'),
                charLevel = smallInput(30, ''),
                charRace = smallInput(30, ''),
                charHome = smallInput(30, ''),
                divHome = document.createElement("div"),
                altName = smallInput(30, ''),
                divName = document.createElement("div"),
                submit = document.createElement('button'),
                closeEditCharacterDiv = document.createElement('div');

            modalStyle = style(named + 'Modal');
            modalStyle("background-color", "#e6f0ff");
            modalStyle.promote();
            window.addEventListener('resize', function() {
                modalStyle("height", (window.innerHeight - 200) + 'px');
                modalStyle.promote();
            });
            
            modalDivStyle = style('modalDiv');
            modalDivStyle.toggle(modalDiv);
            
            
            editCharacterTitle.appendChild(document.createTextNode('Manage Character Lore'));
            static.div.appendChild(editCharacterTitle);
            closeEditCharacterDiv.appendChild(document.createTextNode("Close Manage Character Lore"));
            closeEditCharacterDiv.appendChild(document.createElement('br'));
            closeEditCharacterDiv.appendChild(document.createElement('br'));
            closeEditCharacterDiv.addEventListener('mousedown', function() {
                parent.removeChild(static.div);
                refresh();
            }, true);
            static.div.appendChild(closeEditCharacterDiv);

            hyperlink(closeEditCharacterDiv, static);
            modalDiv.appendChild(document.createTextNode('Secret Key: '));
            modalDiv.appendChild(secretKey);
            modalDiv.appendChild(document.createElement('br'));

            modalDiv.appendChild(document.createTextNode('Official Name: '));
            modalDiv.appendChild(charName);
            modalDiv.appendChild(document.createElement('br'));

            notes.rows = '20';
            notes.cols = '50';
            modalDiv.appendChild(document.createTextNode('Notes: '));
            modalDiv.appendChild(document.createElement('br'));
            modalDiv.appendChild(notes);
            modalDiv.appendChild(document.createElement('br'));

            modalDiv.appendChild(document.createTextNode('Level: '));
            modalDiv.appendChild(charLevel);
            modalDiv.appendChild(document.createElement('br'));

            modalDiv.appendChild(document.createTextNode('Race: '));
            modalDiv.appendChild(charRace);
            modalDiv.appendChild(document.createElement('br'));

            divHome.appendChild(document.createTextNode('Home: '));
            divHome.appendChild(charHome);
            divHome.appendChild(document.createElement('br'));
            modalDiv.appendChild(divHome);
            var homes = multiInput(charHome, divHome);

            divName.appendChild(document.createTextNode('Alternate Names: '));
            divName.appendChild(altName);
            modalDiv.appendChild(divName);
            var names = multiInput(altName, divName);

            submit.appendChild(document.createTextNode('Submit MERP Character Data'));
            modalDiv.appendChild(submit);

            modalDiv.addEventListener('mouseover', function() {
                static.toggle(true);
            }, true);
            modalDiv.addEventListener('mouseout', function() {
                static.toggle(false);
            }, true);

            var id;
            submit.addEventListener("mousedown", function() {
                var merpCharacter = {
                    name: charName.value,
                    notes: notes.value,
                    level: charLevel.value
                }
                if (id) {
                    merpCharacter.id = id;
                }
                var service = ajax(function(response) {
                    id = response.response;
                    var raceService = ajax(function() {});
                    raceService.init('GET', '/rest/merp/' + secretKey.value + '/' + id + '/' + charRace.value);
                    raceService.send();
                    var homeService = ajax(function() {});
                    homeService.init('POST', '/rest/merp/homes/' + secretKey.value + '/' + id);
                    homeService.header('Content-Type', 'application/json;charset=UTF-8');
                    var homeValues = [charHome.value];
                    var wall = homes();
                    while (wall.length) {
                        var value = wall.shift().value;
                        if (value !== '') {
                            homeValues.push(value);
                        }
                    }
                    homeService.send(JSON.stringify(homeValues));

                    var nameService = ajax(function() {});
                    nameService.init('POST', '/rest/merp/names/' + secretKey.value + '/' + id);
                    nameService.header('Content-Type', 'application/json;charset=UTF-8');
                    var nameValues = [altName.value];
                    wall = names();
                    while (wall.length) {
                        var value = wall.shift().value;
                        if (value !== '') {
                            nameValues.push(value);
                        }
                    }
                    nameService.send(JSON.stringify(nameValues));
                });
                service.init('POST', '/rest/merp/' + secretKey.value);
                service.header('Content-Type', 'application/json;charset=UTF-8');
                service.send(JSON.stringify(merpCharacter));

            }, true);

            static.div.appendChild(modalDiv);
            parent.appendChild(static.div);
            static.bringForward();
            
            var height = static.div.offsetHeight;
            modalDivStyle("overflow-y", "scroll");
            modalDivStyle("height", height-175 + "px");
            modalDivStyle.promote();
            
            document.addEventListener(named + 'ModalResize', function(event){
                height = static.div.offsetHeight;
                modalDivStyle("height", height-175 + "px");
                modalDivStyle.promote();
            }, true);
            
            window.addEventListener('resize', function(){
                height = static.div.offsetHeight;
                modalDivStyle("height", height-175 + "px");
                modalDivStyle.promote();
            }, true);
            
            if(character){
                id = character.id;
                charName.value = character.name;
                var charService = ajax(function(response){
                    var toon = JSON.parse(response.response);
                    charLevel.value = toon.level;
                    notes.value = toon.notes;
                });
                charService.init('GET', '/rest/merp/' + id);
                charService.send();
                var raceService = ajax(function(response) {
                    var race = JSON.parse(response.response);
                    charRace.value = race.race.race;
                });
                raceService.init('GET', '/rest/merp/race/'+id);
                raceService.send();
                var homeService = ajax(function(response) {                    
                    var shomes = JSON.parse(response.response),
                        keep = [];
                    var first = shomes.shift();
                    charHome.value = first.home.home;
                    shomes.forEach(function(toonHome){
                        keep.push(toonHome.home.home)
                    })
                    keep.push('');
                    homes(keep);
                });
                homeService.init('GET', '/rest/merp/homes/'+id);
                homeService.send();
                var nameService = ajax(function(response) {
                    var snames = JSON.parse(response.response),
                        keep = [];                    
                    var first = snames.shift();
                    altName.value = first.name;
                    snames.forEach(function(toonName){
                        keep.push(toonName.name)
                    })
                    keep.push('');
                    names(keep);
                });
                nameService.init('GET', '/rest/merp/names/'+id);
                nameService.send();
            }
        }
        newCharacterDiv.addEventListener("mousedown", function() {
            populateManageCharacter();
        }, true);
        
        
    }
});